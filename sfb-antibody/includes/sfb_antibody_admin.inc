<?php

function sfb_antibody_admin() {
  $form = array();

  $form['fieldset-defaults'] = array(
    '#type' => 'fieldset',
    '#title' => '<span class="fa fa-info"></span> ' . t('Defaults'),
    '#description' => '',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['fieldset-defaults']['ac_config_datasheet_public'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow download of PDF datasheets for anonymous users'),
    '#default_value' => variable_get(SFB_ANTIBODY_CONFIG_DATASHEET_PUBLIC, 0),
    '#description' => '',
  );

  $form['fieldset-defaults'][SFB_ANTIBODY_CONFIG_JSON_PUBLIC] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow download of JSON datasheets for anonymous users'),
    '#default_value' => variable_get(SFB_ANTIBODY_CONFIG_JSON_PUBLIC, 0),
    '#description' => '',
  );

  $form['fieldset-defaults'][SFB_ANTIBODY_CONFIG_DEFAULT_SHARING_LEVEL] = array(
    '#type' => 'select',
    '#title' => t('Default sharing level for new antibodies'),
    '#options' => AntibodySharingLevel::getAsArray(),
    '#default_value' => variable_get(SFB_ANTIBODY_CONFIG_DEFAULT_SHARING_LEVEL, AntibodySharingLevel::GROUP_LEVEL),
    '#description' => 'Change default sharing level for new antibodies',
  );

  $form['fieldset-defaults'][SFB_ANTIBODY_CONFIG_OVERVIEW_NO_OF_ANTIBODIES] = array(
    '#type' => 'textfield',
    '#title' => t('Number of antibodies to be shown on primary and secondary overview pages'),
    '#default_value' => variable_get(SFB_ANTIBODY_CONFIG_OVERVIEW_NO_OF_ANTIBODIES, 25),
    '#description' => 'Number of antibodies to be shown on primary and secondary overview pages',
    '#element_validate' => array('element_validate_integer_positive'),
  );

  $form['fieldset-defaults'][SFB_ANTIBODY_CONFIG_ANTIBODYREGISTRY_URL] = array(
    '#type' => 'textfield',
    '#title' => t('Antibody Registry URL (prefix)'),
    '#description' => '', //TODO: description
    '#default_value' => variable_get(SFB_ANTIBODY_CONFIG_ANTIBODYREGISTRY_URL, 'http://antibodyregistry.org/search?q=')
  );

  $form['fieldset-pid'] = array(
    '#type' => 'fieldset',
    '#title' => '<span class="fa fa-info"></span> ' . t('Persistent Identifier Settings'),
    '#description' => '',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['fieldset-pid'][SFB_ANTIBODY_CONFIG_PID_REGEX] = array(
    '#type' => 'textfield',
    '#title' => t('Regular expression returning the aligned Antibody ID from PID'),
    '#description' => 'Regular expression which returns the PID from given pid',
    '#default_value' => variable_get(SFB_ANTIBODY_CONFIG_PID_REGEX, '')
  );

  $form['fieldset-pid'][SFB_ANTIBODY_CONFIG_PID_REGEX_ID_INDEX] = array(
    '#type' => 'textfield',
    '#title' => t('Regex ID index'),
    '#description' => '',
    '#default_value' => variable_get(SFB_ANTIBODY_CONFIG_PID_REGEX_ID_INDEX, 0)
  );

  $form['fieldset-pid'][SFB_ANTIBODY_CONFIG_PID_PRIMARY_PATTERN] = array(
    '#type' => 'textfield',
    '#title' => t('PID pattern for primary antibodies'),
    '#description' => 'String containing {type} and {PID} with pattern for creating PIDs.',
    '#default_value' => variable_get(SFB_ANTIBODY_CONFIG_PID_PRIMARY_PATTERN, '{type}-{PID}'),
  );

  $form['fieldset-pid'][SFB_ANTIBODY_CONFIG_PID_SECONDARY_PATTERN] = array(
    '#type' => 'textfield',
    '#title' => t('PID pattern for secondary antibodies'),
    '#description' => 'String containing {type} and {PID} with pattern for creating PIDs.',
    '#default_value' => variable_get(SFB_ANTIBODY_CONFIG_PID_SECONDARY_PATTERN, '{type}-{PID}'),
  );

  $form['fieldset-pid'][SFB_ANTIBODY_CONFIG_PID_ID_PADDING] = array(
    '#type' => 'textfield',
    '#title' => t('ID padding'),
    '#description' => 'If you wish to use IDs in PID with fixed width, then enter the width ( greater than 0) of the PID ID.',
    '#default_value' => variable_get(SFB_ANTIBODY_CONFIG_PID_ID_PADDING, 4),
    '#element_validate' => array('element_validate_integer'),
  );

  $form['fieldset-pid'][SFB_ANTIBODY_CONFIG_PID_EPIC_URL] = array(
    '#type' => 'textfield',
    '#title' => t('EPIC PID url prefix'),
    '#description' => '', //TODO: description
    '#default_value' => variable_get(SFB_ANTIBODY_CONFIG_PID_EPIC_URL, '')
  );

  $form['fieldset-style'] = array(
    '#type' => 'fieldset',
    '#title' => '<span class="fa fa-info"></span> ' . t('Style settings'),
    '#description' => '',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['fieldset-style'][SFB_ANTIBODY_CONFIG_STYLE_QUALITY_WIDTH] = array(
    '#type' => 'textfield',
    '#title' => t('Quality column width (in px)'),
    '#default_value' => variable_get(SFB_ANTIBODY_CONFIG_STYLE_QUALITY_WIDTH, 100),
    '#description' => '',
    '#element_validate' => array('element_validate_integer_positive'),
  );

  $form['fieldset-applications'] = array(
    '#type' => 'fieldset',
    '#title' => '<span class="fa fa-info"></span> ' . t('Applications'),
    '#description' => '',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $applications = AntibodyApplicationsRepository::findAll();
  $applications_options = array();
  foreach($applications as $app) {
    $applications_options[$app->getAbbreviation()] = $app->getName().' ('.$app->getAbbreviation().')';
  }

  $form['fieldset-applications'][SFB_ANTIBODY_CONFIG_WB_BAND_APPLICATIONS] = array(
    '#type' => 'checkboxes',
    '#options' => $applications_options,
    '#title' => t('Select applications which need WB-BAND field in \'Add/Edit application\' form:'),
    '#default_value' => variable_get(SFB_ANTIBODY_CONFIG_WB_BAND_APPLICATIONS, array()),
  );

  return system_settings_form($form);
}

/**
 * Antibodies overview with options to see logs, open or edit any antibody.
 *
 * This functions is a handler for more than one page:
 *   /manage
 *     Contains a list of all antibodies
 *   /manage/{antibody_id}
 *     NOT YET IMPLEMENTED
 *   /manage/{antibody_id}/logs
 *     Lists all logs entries related to the antibody with {antibody_id}
 *
 * @return string
 */
function sfb_antibody_admin_manage_antibodies() {

  // html output variable
  $output = '';

  // 5th url element may contain antibody id (see function comments)
  $antibody_id = arg(5);

  if($antibody_id == NULL) {

    // if antibody_id is not set, then print a list containing all antibodies
    // with view, edit and logs links

    $tbl_header = array('ID', 'PID', 'Sharing Level', 'Created', 'Last modified', '#Views / #Anon.Views', 'Actions');
    $tbl_rows = array();

    $antibodies = AntibodiesRepository::findAll();

    foreach($antibodies as $antibody) {
      $counter = $antibody->getCounter();

      $tbl_rows[] = array(
        $antibody->getId(),
        $antibody->getElementPID(),
        $antibody->getSharingLevelName(),
        $antibody->getCreatedBy(),
        $antibody->getLastModified(),
        $counter['views'].' / '.$counter['anonymous_views'],
        l('View', $antibody->getViewUrl()) . ', '.
        l('Edit', $antibody->getEditUrl()) . ', '.
        l('Logs ('.count($antibody->getLogs()).')',sfb_antibody_url(SFB_ANTIBODY_URL_CONFIG_MANAGE_ANTIBODY_LOGS, $antibody->getId())),
      );
    }

    $output = theme('table', array('header' => $tbl_header, 'rows' => $tbl_rows));

  } else {

    // if antibody_id is set then print the details of the antibody (NOT YET IMPLEMENTED)
    //TODO: implement single antibody view

    $antibody = AntibodiesRepository::findById($antibody_id);
    if($antibody->isEmpty()){
      drupal_not_found();
      exit();
    }

    // 6th element of url may contain an option like 'logs'
    $option = arg(6);
    if($option == 'logs') {

      // print a list of log entries

      $logs = $antibody->getLogs();

      $tbl_rows = array();

      foreach($logs as $log) {
        $user_name = 'Unknown';
        $user = $log->getUser();
        if(!$user->isEmpty())
          $user_name = $user->getFullname(true);

        $tbl_rows[] = array(
          $log->getId(),
          $log->getDateTime(),
          $log->getActionType(),
          $log->getMessage(),
          $user_name,
        );
      }

      $output = theme('table', array('header' => array('id', 'Date', 'Action', 'Message', 'User'), 'rows' => $tbl_rows));


    } else {
      drupal_not_found();
      exit();
    }

  }

  return $output;
}

/**
 * Species overview
 *
 * @return string
 */
function sfb_antibody_admin_manage_entities() {

  $tbl_rows = array();

  //
  // Species
  //

  $output = '<h2>Species</h2>';

  $all_species = AntibodySpeciesRepository::findAll();
  foreach($all_species as $species) {
    $tbl_rows[] = array(
      $species->getId(),
      $species->getName(),
      '<a href="'.sfb_antibody_url(SFB_ANTIBODY_URL_CONFIG_MANAGE_ENTITIES_SPECIES, $species->getId()).'">Edit</a>'
    );
  }

  $output .= theme('table', array('header' => array('ID', 'Name', 'Options'), 'rows' => $tbl_rows));

  //
  // Applications
  //

  $output .= '<h2>Applications</h2>';

  $all_applications = AntibodyApplicationsRepository::findAll();
  $tbl_rows = array();

  foreach($all_applications as $application) {
    $tbl_rows[] = array(
      $application->getAbbreviation(),
      $application->getName(),
      '<a href="'.sfb_antibody_url(SFB_ANTIBODY_URL_CONFIG_MANAGE_ENTITIES_APPLICATION, $application->getAbbreviation()).'">Edit</a>'
    );
  }

  $output .= theme('table', array('header' => array('Abbreviation', 'Name', 'Options'), 'rows' => $tbl_rows));

  return $output;
}

/**
 * Drupal edit form for species editing
 *
 *
 * @param $form
 * @param $form_state
 * @param int $species_id
 * @return mixed
 */
function sfb_antibody_admin_manage_entity_species($form, &$form_state, $species_id = AntibodySpecies::EMPTY_ANTIBODY_SPECIES_ID) {

  $species = AntibodySpeciesRepository::findById($species_id);

  if($species->isEmpty()) {
    return $form;
  }

  $form_state['storage']['species_id'] = $species_id;

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => $species->getName(),
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Submit handler for drupal edit species form
 *
 * @param $form
 * @param $form_state
 */
function sfb_antibody_admin_manage_entity_species_submit($form, &$form_state) {

  $species_id = $form_state['storage']['species_id'];

  $species = AntibodySpeciesRepository::findById($species_id);
  if(!$species->isEmpty()) {
    $species->setName($form_state['values']['name']);
    AntibodySpeciesRepository::update($species);
    drupal_set_message('Species <strong>'.$species->getName().'</strong> successfully updated.');
    $form_state['redirect'] = sfb_antibody_url(SFB_ANTIBODY_URL_CONFIG_MANAGE_ENTITIES);
  } else {
    drupal_set_message('An error occurred.');
  }
}

/**
 * Drupal edit form for application editing
 *
 * @param $form
 * @param $form_state
 * @param string $application_abbr
 * @return mixed
 */
function sfb_antibody_admin_manage_entity_application($form, &$form_state, $application_abbr = '') {

  $application = new AntibodyApplication();
  $abbreviation_disabled = false;
  $form_state['storage']['app_abbreviation'] = 'add-new';

  if($application_abbr != 'add-new') {

    $application = AntibodyApplicationsRepository::findByAbbreviation($application_abbr);
    $abbreviation_disabled = true;

    if($application->isEmpty()) {
      drupal_set_message('Application could not be found.');
      return $form;
    }

    $form_state['storage']['app_abbreviation'] = $application->getAbbreviation();
  }

  $form['abbreviation'] = array(
    '#type' => 'textfield',
    '#title' => t('Abbreviation'),
    '#default_value' => $application->getAbbreviation(),
    '#maxlength' => 20,
    '#required' => TRUE,
    '#disabled' => $abbreviation_disabled,
  );

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => $application->getName(),
    '#maxlength' => 128,
    '#required' => FALSE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;

}

/**
 * Submit handler for drupal edit form for applications
 *
 * @param $form
 * @param $form_state
 */
function sfb_antibody_admin_manage_entity_application_submit($form, &$form_state) {

  $app_abbreviation = $form_state['storage']['app_abbreviation'];

  $application = AntibodyApplicationsRepository::findByAbbreviation($app_abbreviation);

  if($app_abbreviation == 'add-new') {

    // check whether application already exists
    if($application->isEmpty()) {
      $application = new AntibodyApplication();
      $application->setName($form_state['values']['name']);
      $application->setAbbreviation($form_state['values']['abbreviation']);
      $application->save();
    } else {
      drupal_set_message('You tried to create new application althought application with abbreviation '.$app_abbreviation.' already exists. Changed has not been updated!');
    }

    $form_state['redirect'] = sfb_antibody_url(SFB_ANTIBODY_URL_CONFIG_MANAGE_ENTITIES);

  } else {

    if(!$application->isEmpty()) {
      $application->setName($form_state['values']['name']);
      $application->save();
      drupal_set_message('Application <strong>'.$application->getAbbreviation().'</strong> successfully updated.');
      $form_state['redirect'] = sfb_antibody_url(SFB_ANTIBODY_URL_CONFIG_MANAGE_ENTITIES);
    } else {
      drupal_set_message('An error occurred.');
    }

  }


}