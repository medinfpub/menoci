# Research Data Platform Commons

### Third Party Libraries
* [FPDI](https://www.setasign.com/fpdi), MIT license
* [PHPExcel](https://github.com/PHPOffice/PHPExcel), GNU LGPL
* [TCPDF](http://www.tcpdf.org), GNU LGPL
* [select2](https://select2.org/), MIT license
* [Plotly](https://plot.ly), MIT license ([GitHub](https://github.com/plotly/plotly.js/))