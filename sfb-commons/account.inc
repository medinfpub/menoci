<?php

module_load_include('inc', 'sfb_commons', 'utils');

/**
 * Enable (set to TRUE) if auto-login should be shortened if too long.
 */
define("AUTO_LOGIN_SHORTEN_ON_MAX_LENGTH_ENABLED", FALSE);

/**
 * Maximum length of non-shortened auto-logins.
 */
define("AUTO_LOGIN_MAX_LENGTH", 20);

/**
 * Default expression to strip titles from surname in @see generateLogin() .
 */
define("AUTO_LOGIN_NAME_EXPRESSION", '/^(?<name>[^,]+)/i');

/**
 * Generate a login-id based on first name and surname.
 * If enabled, the login will be shortended if too long.
 *
 * @param string $firstname The first name
 * @param string $surname The surname
 * @return string Generated login id
 */
function generateLoginID($firstname, $surname, $stripTitles = TRUE, $stripTitlesNameExpression = AUTO_LOGIN_NAME_EXPRESSION, $shorten = AUTO_LOGIN_SHORTEN_ON_MAX_LENGTH_ENABLED, $max = AUTO_LOGIN_MAX_LENGTH) {
	$firstname = trim($firstname);
	$surname = trim($surname);
	
	// Strip titles if there are any
	if($stripTitles && preg_match($stripTitlesNameExpression, $surname, $m)) {
		$surname = trim($m['name']);
	}

	$login = sprintf('%s.%s', unidecode($firstname, TRUE), unidecode($surname, TRUE)); // Convert login to ANSI-String

	// Shorten login if too long
	if($shorten && (strlen($login) > $max)) {
		$login = sprintf('%s.%s', substr(unidecode($firstname, TRUE), 0, 1), unidecode($surname, TRUE));
	}

	return $login;
}

/**
 * Create a login id by firstname and surename in format "firstname.lastname" if $login is empty.
 *
 * @param string $firstname The user's firstname
 * @param string $surname The user's surname
 * @param string $login The login or empty of login should be generated.
 * @return string The validated login or generated login if applicable, retuns NULL on failure
 */
function getLoginIDFromCredentials($firstname, $surname, $login = '') {
	$login = trim($login);

	// Generate login if empty in format "firstname.lastname"
	if(empty($login)) {
		$login = generateLoginID($firstname, $surname);
	}

	// Remove all non-alphabet characters (except ".")
	$login = preg_replace("/[^A-Za-z\.]/i", "", $login);

	return empty($login) ? NULL : $login;
}