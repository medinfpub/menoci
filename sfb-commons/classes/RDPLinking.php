<?php
/**
 * @file
 * Contains the RDPLinking class.
 *
 * @author Bartlomiej Marzec (bartlomiej.marzec@med.uni-goettingen.de)
 */


/**
 * Class RDPLinking
 */
class RDPLinking {

  /* ---------------------------- class members ----------------------------- */

  /**
   * Machine name of module that supports linking functions
   *
   * @var string
   */
  private $calleeModuleMachineName;

  /**
   * Table (entity) that can be linking through this class
   *
   * @var string
   */
  private $calleeEntityTableName;

  /* ----------------------------- constructor ------------------------------ */
  /**
   * RDPLinking constructor.
   * @param string $calleeModuleMachineName
   *   Machine name of module that supports linking functions
   * @param $calleeEntityTableName
   *   Table (entity) that can be linking through this class
   */
  public function __construct($calleeModuleMachineName, $calleeEntityTableName) {
    $this->calleeModuleMachineName = $calleeModuleMachineName;
    $this->calleeEntityTableName = $calleeEntityTableName;
  }

  /* --------------------------- getter / setter ---------------------------- */


  /* --------------------------- class functions ---------------------------- */

  /**
   * Returns ids of entities, that are linked to entities from caller table with
   * id $caller_id
   *
   * @param string $caller_table
   * 		Table name with entities
   * @param $caller_id
   * 		ID of entity from $caller_table
   *
   * @return array
   * 		An array with IDs linked to the entity with $caller_id
   */
  public function getLinkings($caller_table, $caller_id) {

    // get metadata of linking table
    $linking_table = $this->getOrCreateLinkingTable($caller_table);

    // query database and get IDs of linked items
    $results = db_select($linking_table['table'], 't')
      ->condition('t.'.$caller_table, $caller_id, '=')
      ->fields('t')
      ->execute();

    // prepare return value(s)
    $callee_ids = array();

    // iterate database results and get linked IDs of callee module
    foreach($results as $result) {
      // cast object to array to get values by using array syntax
      $var_arr = get_object_vars($result);
      // add item to return array
      $callee_ids[] = $var_arr[$this->calleeEntityTableName];
    }

    return $callee_ids;
  }

  /**
   * Get metadata (table name, column 1 and column 2) of linking table. This
   * table contains linkages with IDs of callee and caller entities.
   *
   * @param $caller_table
   * @return array
   * 	 An array containing:
   * 	'table' - the name of the table
   *  'column_1' - the name of the column 1 (linking table 1)
   *  'column_2' - the name of the column 2 (linking table 2)
   *
   */
  private function getOrCreateLinkingTable($caller_table) {

    // check if linking table with callee and caller linkings exists
    if(db_table_exists('rdp_linking_'.$this->calleeEntityTableName.'_'.$caller_table)) {
      return array(
        'table' => 'rdp_linking_' . $this->calleeEntityTableName . '_' . $caller_table,
        'column_1' => $this->calleeEntityTableName,
        'column_2' => $caller_table
      );
    } else if(db_table_exists('rdp_linking_'.$caller_table.'_'.$this->calleeEntityTableName)) {
      return array(
        'table' => 'rdp_linking_' . $caller_table . '_' . $this->calleeEntityTableName,
        'column_1' => $caller_table,
        'column_2' => $this->calleeEntityTableName
      );
    }

    //
    // if linking table does not exists, than create one
    //

    // define schema with Schema API
    $schema = array(
      'description' => 'Table with linkages between '.$this->calleeEntityTableName.' and '.$caller_table,
      'fields' => array(
        $this->calleeEntityTableName => array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE),
        $caller_table => array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE),
      ),
      'primary key' => array($this->calleeEntityTableName, $caller_table),
      // foreign keys are not supported by drupal schema api (documentation purposes only)
    );

    // create table defined in the variable $schema
    db_create_table('rdp_linking_' . $this->calleeEntityTableName . '_' . $caller_table, $schema);


    // return metadata about the table
    return array(
      'table' => 'rdp_linking_' . $this->calleeEntityTableName . '_' . $caller_table,
      'column_1' => $this->calleeEntityTableName,
      'column_2' => $caller_table
    );
  }

  /**
   * //TODO: add comments
   *
   * @param $caller_table
   * @param $caller_id
   * @param bool $themed
   * @return mixed|null
   */
  public function getViewData($caller_table, $caller_id, $themed = false) {
    $linked_id = $this->getLinkings($caller_table, $caller_id);

    $data = module_invoke($this->calleeModuleMachineName, 'rdp_linking_'.$this->calleeEntityTableName.'_view_get', $linked_id, $themed);

    if(is_null($data)) {
      return null;
    }

    return $data;
  }

  /**
   * Returns a drupal form field for linking.
   *
   * @param $caller_table
   *   The name of the table name of caller module
   * @param $caller_id
   *   The id of the item (from caller module table), that will be linked.
   *
   * @return array Drupal form field
   */
  public function getEditForm($caller_table, $caller_id) {
    $linked_ids = $this->getLinkings($caller_table, $caller_id);

    $form = module_invoke($this->calleeModuleMachineName, 'rdp_linking_' . $this->calleeEntityTableName . '_edit_get', $linked_ids, []);

    if(is_null($form)) {
      return array(
        '#markup' => 'Linking function has not been implemented yet.',
      );
    }

    return $form;
  }

  /**
   * @param $caller_table
   *   The name of the table name of caller module
   * @param $caller_id
   *   The id of the item (from caller module table), that will be linked.
   * @param $form_value
   *   Submitted form value
   * @return bool
   */
  public function processSubmittedForm($caller_table, $caller_id, $form_value) {

    //
    // Pass form value to the callee process function and
    // get the ids of linked items
    //
    $entity_ids = module_invoke($this->calleeModuleMachineName, 'rdp_linking_'.$this->calleeEntityTableName.'_edit_submit', $form_value);

    if(is_null($entity_ids)) {
      return false;
    }

    //
    // Delete old linked entries
    //
    $linking_table = $this->getOrCreateLinkingTable($caller_table);
    $results = db_delete($linking_table['table'])
      ->condition($caller_table, $caller_id, '=')
      ->execute();

    //
    // store ids of linked entities
    //
    foreach($entity_ids as $id) {

      db_insert($linking_table['table'])
        ->fields(array(
          $caller_table => $caller_id,
          $this->calleeEntityTableName => $id
        ))
        ->execute();

    }

    return true;
  }


  /* -------------------------- static functions ---------------------------- */


  /**
   * Returns RDPLinking instances of all modules implementing rdp_linking hook.
   *
   * @return RDPLinking[]
   * 		An array with RDPLinking instances of modules which are implementing
   *    rdp_linking hook.
   *
   * @see module_implements()
   */
  public static function getAll() {
    $linkings = array();

    // check which modules implements rdp_linking hook
    foreach (module_implements('rdp_linking') as $module) {
      $linkings[] = RDPLinking::getLinkingByName($module);
    }

    return $linkings;
  }


  /**
   * Returns an instance of RDPLinking of $moduleName modules.
   * If $moduleName module does not implement rdp_linking hook, than null will
   * be returned.
   *
   * @param string $calleeModuleName
   * 	 Name of the module which is called.
   *
   * @param string $calleeEntityTable
   *   (Optional). Sets which table (entity) of callee module should be used.
   *   If not set, then default entity will be used for linking.
   *
   * @return \RDPLinking|null
   * 	 Instance of RDPLinking if callee module supports linking function,
   *   null in other case
   *
   * @see RDPLinking
   */
  public static function getLinkingByName($calleeModuleName, $calleeEntityTable = null) {

    $moduleLinkingConfig = module_invoke($calleeModuleName,'rdp_linking');

    // if module does not implement hook_rdp_linking, then return null
    if(is_null($moduleLinkingConfig)) {
      return null;
    }

    // check if the mandatory variables are set
    if(!isset($moduleLinkingConfig[$calleeModuleName.'_module']) ||
      !isset($moduleLinkingConfig[$calleeModuleName.'_tables'])) {
      return null;
    }

    // check if there are entities that can be linked
    if(count($moduleLinkingConfig[$calleeModuleName.'_tables']) == 0) {
      return null;
    }

    // if $calleentity was not set, then return RDPLinking with
    // first (default) table name
    if(is_null($calleeEntityTable)) {
      return new RDPLinking(
        $calleeModuleName,
        $moduleLinkingConfig[$calleeModuleName.'_tables'][0]
      );
    }

    $calleeModuleName = $moduleLinkingConfig[$calleeModuleName.'_module'];
    // if called entity (table) exists in callee module, than return new
    // RDPLinking object
    foreach($moduleLinkingConfig[$calleeModuleName.'_tables'] as $table) {
      if($table == $calleeEntityTable) {
        return new RDPLinking(
          $calleeModuleName,
          $table
        );
      }
    }

    return null;
  }

}