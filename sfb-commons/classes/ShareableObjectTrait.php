<?php

/**
 * Trait ShareableObjectTrait
 *
 * May be used by classes implementing the @see \ShareableObjectInterface
 *
 */
trait ShareableObjectTrait {

  /**
   * @return bool TRUE if access is granted
   */
  public function checkReadPermission() {

    $lvl = $this->getSharingLevel();

    global $user;
    $uid = $user->uid;

    /**
     * If authenticated user is owner, all permission are granted.
     */
    if ($this->getOwnerId() == $uid) {
      return TRUE;
    }

    if ($lvl == SharingLevel::PUBLIC_LEVEL) {
      /**
       * Object is public, grant access.
       */
      return TRUE;
    }
    elseif ($lvl == SharingLevel::SITE_LEVEL) {
      /**
       * Object is visible for all authenticated users,
       * a user ID that is not 0 implicates authenticated user.
       */
      if ($uid > 0) {
        return TRUE;
      }
      else {
        // If user is not logged in, no need to continue testing.
        return FALSE;
      }
    }
    elseif ($lvl == SharingLevel::GROUP_LEVEL) {
      /**
       * Access granted if the user is part of the objects' owning WorkingGroup.
       */
      $wg = WorkingGroupRepository::findById($this->getWorkingGroupId());
      $rdp_user = UsersRepository::findByUid($uid);
      return $rdp_user->isMemberOfWorkingGroup($wg);
    }

    /**
     * Access denied by default.
     */
    return FALSE;

  }

}