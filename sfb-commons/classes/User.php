<?php

class User {

  const USER_NOT_SET = -1;

  const USER_ANONYMOUS_ID = 0;    // current drupal version defines anonymous as user with uid = 0

  private $uid;

  private $name;

  private $mail;

  public function getUid() {
    return $this->uid;
  }

  public function setUid($uid) {
    $this->uid = $uid;
  }

  public function getName() {
    return $this->name;
  }

  public function setName($name) {
    $this->name = $name;
  }

  public function getMail() {
    return $this->mail;
  }

  public function setMail($mail) {
    $this->mail = $mail;
  }

  public function getFullname($commaseparated = FALSE) {
    if ($this->isEmpty()) {
      return '';
    }

    $first_name = $this->getFirstName();
    $last_name = $this->getSurname();

    if ($commaseparated) {
      return $last_name . ', ' . $first_name;
    }

    return $first_name . ' ' . $last_name;
  }

  public function getFullNameWithOrcid() {
    $orcid = $this->getOrcid();
    if (empty($orcid)) {
      return $this->getFullname();
    }

    $orcid = str_replace('https://', '', $orcid);
    $orcid = str_replace('http://', '', $orcid);
    $orcid = str_replace('orcid.org/', '', $orcid);
    $orcid = 'https://orcid.org/' . $orcid;

    $icon = theme_image([
      'path' => base_path() . drupal_get_path('module', 'sfb_commons') . '/resources/orcid_icon.svg',
      'height' => '14px',
      'alt' => 'orcig.org logo',
      'title' => $orcid,
      'attributes' => [],
    ]);
    $string = $this->getFullname();
    $string .= '&nbsp;' . l($icon, $orcid,
        ['html' => TRUE, 'attributes' => ['target' => '_blank']]);
    return $string;
  }

  public function getFirstName() {
    if ($this->isEmpty()) {
      return '';
    }

    $profile = profile2_load_by_user($this->uid, variable_get(SFB_COMMONS_CONFIG_VAR_PROFILE_TYPE));

    $field_machine_name = RDP_COMMONS_FIELD_FIRSTNAME;
    if (isset($profile->$field_machine_name['und'][0]['safe_value'])) {
      return $profile->$field_machine_name['und'][0]['safe_value'];
    }

    return NULL;
  }

  public function getSurname() {
    if ($this->isEmpty()) {
      return '';
    }

    $profile = profile2_load_by_user($this->uid, variable_get(SFB_COMMONS_CONFIG_VAR_PROFILE_TYPE));

    $field_machine_name = RDP_COMMONS_FIELD_SURNAME;
    if (isset($profile->$field_machine_name['und'][0]['safe_value'])) {
      return $profile->$field_machine_name['und'][0]['safe_value'];
    }

    return NULL;
  }

  public function getOrcid() {
    if ($this->isEmpty()) {
      return '';
    }

    $profile = profile2_load_by_user($this->uid, variable_get(SFB_COMMONS_CONFIG_VAR_PROFILE_TYPE));

    $field_machine_name = RDP_COMMONS_FIELD_ORCID;
    if (isset($profile->$field_machine_name['und'][0]['safe_value'])) {
      return $profile->$field_machine_name['und'][0]['safe_value'];
    }

    return NULL;
  }

  public function isEmpty() {
    if ($this->uid == User::USER_NOT_SET || $this->uid == User::USER_ANONYMOUS_ID) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Returns an array with workings groups of this user, defined by assigned
   * roles to this user.
   *
   * Current definition implicates that also group cooperators are members of
   * this group.
   *
   * @param bool $only_shortnames If TRUE, return array will contain only group
   *   shortnames instead of objects
   *
   * @return WorkingGroup[] | string[]
   */
  public function getUserWorkingGroups($only_shortnames = FALSE) {
    $working_groups = WorkingGroupRepository::findAll();
    $groups = [];
    /**
     * Check for each working group instance, if user has roles matching
     * the groups' short name
     */
    foreach ($working_groups as $working_group) {
      $user = user_load($this->getUid());
      // Admin-switch: set TRUE if user has administrator role
      $admin = in_array('administrator', $user->roles) ? TRUE : FALSE;
      foreach ($user->roles as $role) {
        $wg_short_name = $working_group->getShortName();
        // Add WG to list is user is admin or user has role for WG in question
        if ($admin || strchr($role, $wg_short_name)) {
          if (!in_array($wg_short_name, array_keys($groups))) {
            $groups[$wg_short_name] = $working_group;
          }
        }
      }
    }

    /**
     * Return an array of WorkingGroup objects or an array of shortnames based
     * on evaluation of $only_shortname parameter.
     */
    if (!$only_shortnames) {
      return $groups;
    }
    else {
      return array_keys($groups);
    }
  }

  /**
   * Checks whether the user is member of working group.
   *
   * @param $workingGroup WorkingGroup
   *
   * @return bool true if user is member of working group
   */
  public function isMemberOfWorkingGroup($workingGroup) {

    if ($workingGroup->isEmpty()) {
      return FALSE;
    }

    $user_working_groups = $this->getUserWorkingGroups();

    foreach ($user_working_groups as $user_working_group) {
      if ($user_working_group->getId() == $workingGroup->getId()) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Returns current user, defined by global variable $user.
   *
   * @return User
   */
  public static function getCurrent() {
    global $user;
    return UsersRepository::findByUid($user->uid);
  }

  /* --------------------------------------- WARNING - ONLY FOR ECHO MODULE - --------------------------------------- */


  /**
   * Return roles assigned to this user
   *
   * @return \EchoUserRole[]
   * @deprecated
   */
  public function getRoles() {
    return EchoUserRolesRepository::findByUserId($this->uid);
  }

  /**
   * @param \EchoUserRole $role
   *
   * @return bool
   * @deprecated
   */
  public function hasRole($role) {

    $db_roles = $this->getRoles();

    foreach ($db_roles as $db_role) {
      if ($role == $db_role) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * @param $uid
   *
   * @return User
   * @deprecated
   *
   */
  public static function getUser($uid) {
    return UsersRepository::findByUid($uid);
  }

  /**
   * @deprecated
   */
  public static function getResearchGroupName() {
    return '';
  }

}

