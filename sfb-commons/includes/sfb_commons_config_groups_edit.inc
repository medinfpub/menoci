<?php

/**
 * Edit working group data
 *
 * @see drupal_get_form()
 *
 * @return array
 */
function sfb_commons_config_groups_edit($form, &$form_state, $working_group_id) {
  $form = [];

  // get working group from database
  $working_group = WorkingGroupRepository::findById($working_group_id);

  // if working group is empty, than there is no corresponding database entry
  // with given working group id
  if ($working_group->isEmpty()) {
    drupal_not_found();
    return;
  }

  // add working group id into form_state
  // this variable will be used in submit handler
  $form_state['working_group_id'] = $working_group->getId();

  $form['short_name'] = [
    '#type' => 'textfield',
    '#title' => t('Short name'),
    '#description' => t('Research group short name'),
    '#default_value' => $working_group->getShortName(),
    '#attributes' => ['disabled' => 'disabled'],
  ];

  $form['name'] = [
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('Research group name'),
    '#default_value' => $working_group->getName(),
  ];

  $form['leader'] = [
    '#type' => 'textfield',
    '#title' => t('Leader'),
    '#description' => t('Leader'),
    '#default_value' => $working_group->getLeader(),
  ];

  $form['leader_email'] = [
    '#type' => 'textfield',
    '#title' => t('Leader\'s E-mail'),
    '#description' => t('E-mail address'),
    '#default_value' => $working_group->getLeaderEmail(),
  ];

  $form['leader_institution'] = [
    '#type' => 'textfield',
    '#title' => t('Leader\'s institution'),
    '#description' => t('Institution'),
    '#default_value' => $working_group->getLeaderInstitution(),
  ];


  /********************* UPLOAD WORKING GROUP LOGO IMAGES *********************/
  $resources_path = variable_get(SFB_COMMONS_CONFIG_VARIABLE_RESOURCES_PATH);
  $shortname = $working_group->getShortName();
  $logo_sizes = WorkingGroup::getLogoSizes();
  // Initialize fieldset
  $form['logo'] = [
    '#type' => 'fieldset',
    '#title' => t('Upload Research Group logo images.'),
    '#suffix' => '',
  ];
  // File upload fields for different size logos as defined in WorkingGroup class
  foreach ($logo_sizes as $logo_size) {
    $existing_image = $resources_path . $shortname . $logo_size . '.png';
    $form['logo'][$logo_size] = [
      '#prefix' => '<h4 style>' . $logo_size . ' pixel research group logo</h4>
                    <div class="container-inline" style="margin-bottom: 20px;">
                    <div class="col-xs-1"><img src="' . $existing_image . '" style="max-height: 32px;" alt="missing" /></div>
                    <div class="col-xs-11">',
      '#type' => 'file',
      #'#title' => $logo_size . ' pixel research group logo',
      '#suffix' => '</div></div><p>&nbsp;</p>',
    ];
  }
  // File upload field for principal investigator headshot picture
  $existing_image = $resources_path . 'pi_' . $shortname . '.png';
  $form['logo']['pi'] = [
    '#prefix' => '<h4>Research group leader photo</h4>
                  <div class="container-inline">
                  <div class="col-xs-1"><img src="' . $existing_image . '" style="max-height: 32px;" alt="missing" /></div>
                  <div class="col-xs-11">',
    '#type' => 'file',
    #'#title' => 'Research group leader photo',
    '#suffix' => '</div></div>',
  ];

  // submit button
  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Submit'),
  ];

  return $form;
}

/**
 * Validation for group edit form.
 */
function sfb_commons_config_groups_edit_validate($form, &$form_state) {

  $logo_sizes = WorkingGroup::getLogoSizes();

  foreach ($logo_sizes as $logo_size) {
    // filename to store image, for example "ag_name20x20.png"
    $filename = $form_state['values']['short_name'] . $logo_size . '.png';
    /**
     * Save file in custom stream wrapper path, @see \RDPStreamWrapper
     */
    if ($file = _custom_save_image($logo_size, $logo_size, 'rdp://', $filename)) {
      /** Make the file permanently available  */
      $file->status |= FILE_STATUS_PERMANENT;
      $form_state['values'][$logo_size] = file_save($file);
    }
  }

  $filename = 'pi_' . $form_state['values']['short_name'] . '.png';
  /**
   * Save file in custom stream wrapper path, @see \RDPStreamWrapper
   */
  if ($file = _custom_save_image('pi', '187x187', 'rdp://', $filename)) {
    /** Make the file permanently available  */
    $file->status |= FILE_STATUS_PERMANENT;
    $form_state['values']['pi'] = file_save($file);
  }
}

/**
 * Save file in custom stream wrapper path, @see \RDPStreamWrapper
 *
 * @param $fieldname Form fieldname of a file upload type.
 * @param $imagesize Image resolution in format "20x20" pixels.
 * @param string $path Stream wrapper path where to store uploaded file.
 * @param string $filename Filename as which to store uploaded file.
 *
 * @return bool|\stdClass FALSE if something went wrong, stdClass file object
 *   otherwise
 */
function _custom_save_image($fieldname, $imagesize, $path = 'rdp://', $filename = "") {
  /**
   * Only proceed if something was uploaded.
   */
  if (!$_FILES['files']['error'][$fieldname] == UPLOAD_ERR_NO_FILE) {
    /**
     * Make sure only PNG images are accepted and image resolution is enforced.
     */
    $validators = [
      'file_validate_extensions' => ['png'],
      'file_validate_image_resolution' => [$imagesize],
    ];

    /** Try file upload */
    if (!$logo = file_save_upload($fieldname, $validators)) {
      form_set_error($fieldname, t("Failed to upload the :fieldname file.", [':fieldname' => $fieldname]));
    }
    else {
      /** Make sure target upload directory is configured correctly. */
      if (!file_prepare_directory($path, FILE_MODIFY_PERMISSIONS | FILE_CREATE_DIRECTORY)) {
        drupal_set_message(t('Failed to create %directory.', ['%directory' => $path]), 'error');
      }
      if (empty($filename)) {
        /** If no filename was passed, store file under original name. */
        $filename = $logo->filename;
      }
      /** Move uploaded file to target dir. */
      if ($logo = file_move($logo, $path . $filename, FILE_EXISTS_REPLACE)) {
        return $logo;
      }
    }
  }
}

/**
 * Form submit handler
 *
 * @param $form
 * @param $form_state
 */
function sfb_commons_config_groups_edit_submit($form, &$form_state) {

  // get working group object with database data
  $working_group = WorkingGroupRepository::findById($form_state['working_group_id']);

  if ($working_group->isEmpty()) {
    drupal_set_message(t('Something went wrong! Form could not be stored!'), 'error');
    return;
  }

  // set class members with data from the form
  $working_group->setShortName($form_state['values']['short_name']);
  $working_group->setName($form_state['values']['name']);
  $working_group->setLeader($form_state['values']['leader']);
  $working_group->setLeaderEmail($form_state['values']['leader_email']);
  $working_group->setLeaderInstitution($form_state['values']['leader_institution']);

  // store the data into database
  if (!$working_group->save()) {
    drupal_set_message(t('There was a problem with the database. Form could not be stored!'), 'error');
    return;
  }

  // redirect to working groups overview
  $form_state['redirect'] = SFB_COMMONS_URL_CONFIG_WORKINGGROUPS;
  return;
}
