<?php
/**
 * @file Form and action handler to add subprojects.
 *
 * @author Markus Suhr (markus.suhr@med.uni-goettingen.de)
 */

/**
 * Form to add new subproject.
 *
 * @return array Drupal form.
 */
function sfb_commons_config_subprojects_add($form = null, &$form_state) {

  $form = [];

  $subproject = new Subproject();

  $form['abbreviation'] = array(
    '#type' => 'textfield',
    '#title' => t('Abbreviation'),
    '#description' => t('Abbreviation for the subproject'),
    '#default_value' => $subproject->getAbbreviation(),
    '#required' => true,
  );

  $form['status'] = array(
    '#type' => 'select',
    '#title' => t('Status'),
    '#description' => t('Status of the subproject'),
    '#options' => Subproject::$status_options,
    '#default_value' => $subproject->getStatus(),
    '#required' => true,
  );

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('Title of the subproject'),
    '#default_value' => $subproject->getTitle(),
    '#maxlength' => 256,
    '#required' => true,
  );

  // submit button
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Handle sfb_commons_config_subprojects_add submit action.
 * Translate form field into Subproject object and save it to database.
 *
 * @param $form
 * @param $form_state
 */
function sfb_commons_config_subprojects_add_submit($form, &$form_state)
{
  $subproject = new Subproject();

  // set class members with data from the form
  $subproject->setAbbreviation($form_state['values']['abbreviation']);
  $subproject->setStatus($form_state['values']['status']);
  $subproject->setTitle($form_state['values']['title']);

  // store the data into database
  if(!$subproject->save()) {
    drupal_set_message(t('There was a problem with the database. Form could not be stored!'), 'error');
    return;
  }

  // redirect to subprojects overview
  $form_state['redirect'] = SFB_COMMONS_URL_CONFIG_SUBPROJECTS;
  return;
}
