<?php
/**
 * Created by PhpStorm.
 * User: suhr
 * Date: 01.08.18
 * Time: 08:50
 */

abstract class ExternalIDRenderer {

  public static function badge(\ExternalID $xid) {
    $xid_type_label = $xid->getTypeLabel();
    $icon = "<span class=\"glyphicon glyphicon-link\"></span>&nbsp;";
    $label_text = self::trim_url($xid->getValue());
    $label_text = strlen($label_text) > 31 ? self::shorten_url($label_text) : $label_text;
    $label = $icon . $label_text;

    $link = l($label, $xid->url('resolve'),
      [
        'html' => TRUE,
        'external' => TRUE,
        'absolute' => TRUE,
        'attributes' => ['target' => '_blank', 'title' => $xid_type_label],
      ]);

    $class = "ex_id_badge_" . substr(md5($xid_type_label), 0, 8);
    $badge = "<span class=\"$class\">$link</span>";

    $xid_type = ExternalIDTypeRepository::findById($xid->getType());

    /**
     * Fetch HEX color code for badge background
     *
     * @var \ExternalIDType $xid_type
     */
    $background_color = '#' . $xid_type->getDisplayColorCode();

    self::add_badge_css($class, $background_color);

    return $badge;
  }

  private static function add_badge_css($class, $background_color) {
    $css = /** @lang CSS */
      "span." . $class . " { 
        background-color: " . $background_color . "; 
        display: inline-block; 
        border-radius: 5px;
        color: white;
        padding-left: 4px;
        padding-right: 4px;
        font-size: 70%;
        font-weight: bolder;
        margin: 4px;
       }
       span." . $class . ":hover {
        cursor: pointer;
        opacity: 0.8;
       }
       span." . $class . ">a {
        color: unset;
       }
       span." . $class . ">a:hover {
        text-decoration: none;
       }";
    drupal_add_css($css, ['type' => 'inline']);
  }

  private static function trim_url($url) {
    $url = str_replace("http://", "", $url);
    $url = str_replace("https://", "", $url);
    $url = str_replace("www.", "", $url);
    return $url;
  }

  private static function shorten_url($url) {
    return substr($url, 0, 8) . '...' . substr($url, -6, 6);
  }

}