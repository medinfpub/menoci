<?php

module_load_include('inc', 'sfb_commons', 'utils');
module_load_include('inc', 'sfb_literature', 'includes/common');
module_load_include('php', 'sfb_literature', 'includes/export');

/**
 * Translates accented Latin characters to their base-form and replaces all Unicode codepoints that
 * are not in the ranges {U+002C - U+002E, U+0030 - U+0039, U+0041 - U+005A, U+0061 - U+007A, U+005F}
 * with an underscore-character ('_'; Codepoint U+005F). The unchanged codepoints correspond to
 * the characters ',', '-', '.', '0' - '9', 'A' - 'Z', 'a' - 'z', and '_'.
 *
 * @param $str The string to convert.
 * @return The converted string.
 */
function unidecodehard($str/*, $setlocale='de_DE'*/) {
	$str = iconv('UTF-8', 'ISO-8859-1//TRANSLIT', $str);
	$str = preg_replace('/[^\x2C-\x2E\x30-\x39\x41-\x5A\x61-\x7A\x5F]/','_', $str); // ',', '-', '.', '0' - '9', 'A' - 'Z', 'a' - 'z', '_'

	return $str;
}

function sfb_literature_export() {
	$result = NULL;

	try {
		// Query database
		$q = db_select('pubreg_articles', 'a')
		     ->fields('a')
		     ->orderBy('id', 'ASC')
		     ->execute();

		// Retrieve rows
		$result = $q->fetchAllAssoc('id');
	} catch(Exception $e) {
		drupal_set_message('An database error has occurred.', 'error');
		drupal_access_denied();
	}

  module_load_include('inc', 'sfb_literature', 'includes/session');
  $publications_filter = lists_session("publications_filter");

	$c = count($result);

	$t = SpreadsheetExporter::FORMAT_XLSX;

	$exporter = new SpreadsheetExporter($t);


	drupal_add_http_header('Access-Control-Allow-Origin', '*');
	drupal_add_http_header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');

	$ext = $exporter->GetFileExtention();

	if ($publications_filter == -1) {
    $fname = sprintf('%s_excel_export.%s', date('Y-m-d'), $ext);
  }
	else if ($publications_filter == -2) {
    $fname = sprintf('%s_excel_export_empty.%s', date('Y-m-d'), $ext);
    $result = [];
  }
	else {
    $fname = sprintf('%s_excel_export_filtered.%s', date('Y-m-d'), $ext);
    $result = $publications_filter;
  }

	$mime = $exporter->GetMimeType();

	$ransi = unidecodehard($fname);
	$ru = rawurlencode($fname);

	header(sprintf('Content-Type: %s', $mime));
	header(sprintf('Content-Disposition: attachment; filename="%s"; filename*=UTF-8\'\'%s', $ransi, $ru)); // Filename with safe ansiString encoding and UTF-8 encoded real name
	header('Expires: Thu, 01 Dec 1994 16:00:00 GMT'); // rfc2616
	header(sprintf('Last-Modified: %s GMT', gmdate('D, d M Y H:i:s'))); // New content
	header('Cache-Control: private, max-age=0, no-cache, no-store, must-revalidate');
	header('Pragma: no-cache');

	ob_start();

	$exporter->Export($result);

	$l = ob_get_length();

	if($l != 0) {
		header(sprintf('Content-Length: %u', $l));
	}

	ob_end_flush();

	drupal_exit();
}
