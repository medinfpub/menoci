<?php
/**
 * @file Drupal form engine utilities for Publication class.
 */

/**
 * Class PublicationForm
 */
abstract class PublicationForm {

  public static $options_open_access = [
    0 => 'No',
    1 => 'Yes',
    2 => 'Unknown',
  ];

  /**
   * @param string $field_name
   * @param \Publication $publication_object
   *
   * @return array Drupal form element
   */
  public static function get_form_field(
    $field_name,
    $publication_object = NULL
  ) {
    if (is_null($publication_object)) {
      $publication_object = new Publication();
    }

    $field = [];

    // Needed for select 2 fields to work.
    sfb_commons_add_select2('','','','');
    drupal_add_js('jQuery(document).ready(function() {
            jQuery(".field_select2").select2();
          });', 'inline');

    switch ($field_name) {

      case 'wg':
        $user = User::getCurrent();
        $user_wgs = $user->getUserWorkingGroups();

        $options = [];
        foreach ($user_wgs as $key => $working_group) {
          $options[$working_group->getShortName()] = $working_group->getName();
        }

        asort($options);

        $field = [
          '#type' => 'select',
          '#multiple' => TRUE,
          '#title' => t('Working Groups *'),
          '#options' => $options,
          '#description' => t('Select working groups to assign the newly created publication to.'),
          '#attributes' => [ 'class' => ['field_select2'], 'multiple' => 'multiple'],
        ];
        break;

      case 'subproject':
        $subprojects = SubprojectsRepository::findAll();
        $options = [];

        foreach ($subprojects as $subproject) {
          $options[$subproject->getAbbreviation()] = $subproject->getAbbreviation() . ' - ' . $subproject->getTitle();
        }
        $field = [
          '#type' => 'select',
          '#multiple' => TRUE,
          '#title' => t('Subprojects'),
          '#options' => $options,
          '#description' => t('Select the associated subprojects.'),
          '#attributes' => [ 'class' => ['field_select2'], 'multiple' => 'multiple'],
        ];
        break;

      case 'publication_type':
        $field = [
          '#type' => 'select',
          '#title' => t('Publication Type *'),
          '#options' => Publication::publication_types(),
          '#description' => t('Select the Publication Type.'),
          '#empty_option' => t('-- select publication type --'),
        ];
        if ($publication_object->getPublicationType() !== 'unknown') {
          $field['#default_value'] = $publication_object->getPublicationType();
        }
        break;

      case 'open_access':
        $field = [
          '#type' => 'select',
          '#title' => t('Open Access'),
          '#options' => self::$options_open_access,
          '#default_value' => 2,
          '#description' => t('Select the Open Access status.'),
        ];
        break;

    }
    return $field;
  }
}