# Changelog
All notable changes to this project will be documented in this file. The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project follows [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

Releases may contain the folloging sections: 
* `Added` for new features.
* `Changed` for changes in existing functionality.
* `Deprecated` for soon-to-be removed features.
* `Removed` for now removed features.
* `Fixed` for any bug fixes.
* `Security` in case of vulnerabilities.

## 1.1 - 2021-02-01

### Added

* Upgraded "archive" module to v1.0

## 1.0.1 - 2021-01-29

### Added

* Updates to menoci modules "commons", "literature", "antibody", "mouseline"
* Upgraded "archive" module to v0.7
* Upgraded Drupal base to 7.78

## 1.0 - 2020-02-27

### Added

* Dockerfile
* Included menoci modules "commons", "literature", "antibody", "mouseline", 
"archive", and "wikidata" in the current release versions
* Created a bash script `update.sh` to collect latest module source code from 
original Git repositories for combined release


## [Unreleased] - YYYY-MM-DD

### Added

* Dockerfile
* Included menoci modules "commons", "literature", "antibody", "mouseline", 
"archive", and "wikidata" in the current release versions
* Created a bash script `update.sh` to collect latest module source code from 
original Git repositories for combined release

### Changed

### Deprecated

### Removed

### Fixed

### Security
