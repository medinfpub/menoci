<?php
/**
 * @file
 * ToDo: Short description for file.
 *
 * Long description for file.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class ArchiveObjectRepository
 */
class ArchiveObjectRepository {

  private static $tableName = 'rdp_archive_object';

  private static $databaseFields = [
    'id',
    'archive_id',
    'cdstar_object_id',
    'pid_id',
    'sharing_level',
    'context',
    'working_group_id',
  ];

  public static function getTableName() {
    return self::$tableName;
  }

  public static function getDatabaseFields() {
    return self::$databaseFields;
  }

  /**
   * @param \ArchiveObjectInterface $archiveObject
   *
   * @return int|bool ID of the stored database entry on success, FALSE otherwise
   */
  public static function save(ArchiveObjectInterface $archiveObject) {
    // insert or update database data
    if (isset($archiveObject)) {
      $id = $archiveObject->isEmpty() ? NULL : $archiveObject->getId();

      try {
        $wg_id = $archiveObject->getWorkingGroupID();
        db_merge(self::$tableName)
          ->key(['id' => $id])
          ->fields([
              'archive_id' => $archiveObject->getArchiveId(),
              'cdstar_object_id' => $archiveObject->getCdstarObjectId(),
              'pid_id' => $archiveObject->getPidId(),
              'sharing_level' => $archiveObject->getSharingLevel(),
              'context' => $archiveObject->getContext(),
              'working_group_id' => $wg_id > 0 ? $wg_id : NULL,
            ]
          )->execute();

        $id = is_null($id) ? Database::getConnection()->lastInsertId() : $id;
        return $id;
      } catch (Exception $e) {

        watchdog_exception('rdp_archive', $e);
        drupal_set_message($e->getMessage(), 'error');
      }
    }
    return FALSE;
  }

  /**
   * @param int $archive_object_id
   *
   * @return bool
   */
  public static function deleteById($archive_object_id) {
    $archiveObject = ArchiveObjectRepository::findById($archive_object_id);
    return self::delete($archiveObject);
  }

  /**
   * @param int $id
   *
   * @return \ArchiveObject
   */
  public static function findById($id) {
    $result = db_select(self::$tableName, 't')
      ->fields('t', self::$databaseFields)
      ->condition('id', $id)
      ->execute()
      ->fetch();

    return self::databaseResultToObject($result);
  }

  /**
   * @param $result
   *
   * @return \ArchiveObject
   */
  private static function databaseResultToObject($result) {
    $archiveObject = new ArchiveObject();

    if (empty($result)) {
      return $archiveObject;
    }

    $archiveObject->setId($result->id);
    $archiveObject->setArchiveId($result->archive_id);
    $archiveObject->setCdstarObjectId($result->cdstar_object_id);
    $archiveObject->setPidId($result->pid_id);
    $archiveObject->setSharingLevel($result->sharing_level);
    $archiveObject->setContext($result->context);
    $archiveObject->setWorkingGroupID($result->working_group_id);

    return $archiveObject;
  }

  /**
   * @param \ArchiveObjectInterface $archiveObject
   *
   * @return bool TRUE if \ArchiveObject instance has been successfully deleted, FALSE otherwise
   */
  public static function delete(ArchiveObjectInterface $archiveObject) {

    /**
     * Initiate return value
     */
    $success = FALSE;

    /**
     * Collect info for log messages
     */
    $name = $archiveObject->getDisplayName();
    $id = $archiveObject->getId();

    if ($archiveObject->hasPid()) {
      drupal_set_message($msg = t("Cannot delete ArchiveObject that has a
      Persistent Identifier assigned to it, ID: %id.", ['%id' => $id]), 'error');
      watchdog(__CLASS__, "Cannot delete ArchiveObject that has a
      Persistent Identifier assigned to it, ID: %id.", ['%id' => $id], WATCHDOG_INFO);
    }
    else {
      $transaction = db_transaction();
      try {
        /**
         * First, delete CdstarObject.
         */
        if (!CdstarObjectRepository::deleteById($archiveObject->getCdstarObjectId())) {
          throw new Exception('Delete CdstarObject failed.');
        }

        /**
         * Delete auxiliary ArchiveObject data.
         */
        db_delete(self::$tableName)
          ->condition('id', $archiveObject->getId())
          ->execute();

        /**
         * Reset object parameters
         */
        $archiveObject->setId(ArchiveObjectInterface::EMPTY_ID);
        $archiveObject->setCdstarObjectId(CdstarObject::EMPTY_ID);

        $success = TRUE;

      } catch (Exception $exception) {
        // handle Exception
        watchdog_exception(__CLASS__, $exception);

        $transaction->rollback();
      }

      if ($success) {
        watchdog(__CLASS__, "Successfully deleted ArchiveObject %n, ID: %id", ['%n' => $name, '%id' => $id],
          WATCHDOG_INFO);
      }
      else {
        watchdog(__CLASS__, " Failed to delete ArchiveObject %n, ID: %id.", ['%n' => $name, '%id' => $id],
          WATCHDOG_ERROR);
      }
    }

    return $success;
  }

  /**
   * @return \ArchiveObject[]
   */
  public static function findAll() {
    $results = db_select(self::$tableName, 't')
      ->fields('t', self::$databaseFields)
      ->execute()
      ->fetchAll();

    return self::databaseResultsToObjects($results);
  }

  /**
   * Map database results array to an array of \ArchiveObject instances
   *
   * @param array $results
   *
   * @return \ArchiveObject[]
   */
  protected static function databaseResultsToObjects($results) {
    $objects = [];
    foreach ($results as $result) {
      $objects[] = self::databaseResultToObject($result);
    }

    return $objects;
  }

  /**
   * Return an array of all \ArchiveObject instances that are associated with a certain \Archive (identified by
   * $archive_id parameter)
   *
   * @param int $archive_id
   *
   * @return \ArchiveObject[]
   */
  public static function findAllByArchiveId($archive_id) {
    $results = db_select(self::$tableName, 't')
      ->fields('t', self::$databaseFields)
      ->condition('archive_id', $archive_id)
      ->execute()
      ->fetchAll();

    return self::databaseResultsToObjects($results);
  }

  /**
   * @param int $level An integer coding a valid sharing level as defined by \SharingLevel interface
   *
   * @return \ArchiveObject[]
   */
  public static function findAllBySharingLevel($level) {
    $results = db_select(self::$tableName, 't')
      ->fields('t', self::$databaseFields)
      ->condition('sharing_level', $level)
      ->execute()
      ->fetchAll();

    return self::databaseResultsToObjects($results);
  }

  /**
   * @param string $context
   *
   * @return \ArchiveObject[]
   */
  public static function findAllByContext($context) {
    $results = db_select(self::$tableName, 't')
      ->fields('t', self::$databaseFields)
      ->condition('context', $context)
      ->execute()
      ->fetchAll();

    return self::databaseResultsToObjects($results);
  }

  /**
   * @return \ArchiveObject|bool A created \ArchiveObject on success, FALSE otherwise
   */
  public static function processTusUpload() {
    $upload_response = $_POST;

    if (!empty($upload_response['tus_upload_data'])) {

      $archive_id = ArchiveObjectForm::getUserPersonalArchiveID();
      $archive = ArchiveRepository::findById($archive_id);
      $cdstar_server = CdstarServerRepository::findById($archive->getServer());

      $uploaded_files = json_decode($upload_response['tus_upload_data'], TRUE);
      $cdstarObject = $cdstar_server->createObjectFromTusUpload($uploaded_files);

      $archive_object = new ArchiveObject();
      $archive_object->setArchiveId($archive_id);
      $archive_object->setCdstarObjectId($cdstarObject->getId());
      $archive_object->save();

      return $archive_object;
    }
    else {
      return FALSE;
    }
  }
}