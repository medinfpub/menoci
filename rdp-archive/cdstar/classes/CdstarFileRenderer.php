<?php


class CdstarFileRenderer {

  /**
   * @param \CdstarFile $cdstarFile
   * @param string $base_route
   * @param bool $with_delete_button
   *
   * @return string
   *
   */
  public static function display_tile(CdstarFile $cdstarFile, string $base_route, $with_delete_button = FALSE) {
    // Get an icon for the file-type (needs a fake file-object for theme)
    $file = new stdClass();
    $file->filemime = $cdstarFile->getContentType();
    $mime_icon = theme_file_icon([
      'file' => $file,
      'alt' => $cdstarFile->getContentType(),
      'icon_directory' => NULL,
    ]);

    $download_icon = l(CdstarResources::GLYPHICON_DOWNLOAD,
      $base_route . $cdstarFile->getId() . '/download', ['html' => TRUE]);

    $delete_icon = $with_delete_button ? '&nbsp;&nbsp;' . l(CdstarResources::GLYPHICON_DELETE,
        $base_route . $cdstarFile->getId() . '/delete', ['html' => TRUE]) : '';

    $class_left = 'col-md-4';
    $class_right = 'col-md-8';


    $output = '<div class="col-sm-6">
                    <div class="container-fluid table-bordered" 
                        style="font-size: smaller; padding: 2px; margin: 2px; background-color: #f5f5f5;">
                        <div class="col-xs-12">' . $mime_icon . '&nbsp;<strong>'
      . $cdstarFile->getFilename() . '</strong> ['
      . format_size($cdstarFile->getFilesize())
      . ']&nbsp;' . $download_icon . '&nbsp;'
      . $delete_icon . '</div>
                        <div class="' . $class_left . '">Created:</div>
                        <div class="' . $class_right . '">' . date("Y-m-d H:i:s", $cdstarFile->getCreated()) . '</div>
                        <div class="' . $class_left . '">Last modified:</div>
                        <div class="' . $class_right . '">' . date("Y-m-d H:i:s", $cdstarFile->getLastModified()) . '</div>
                        <div class="' . $class_left . '">Checksum:</div>
                        <div class="' . $class_right . '">' . $cdstarFile->getChecksum() . '</div>
                    </div>
                   </div>';
    return $output;
  }
}