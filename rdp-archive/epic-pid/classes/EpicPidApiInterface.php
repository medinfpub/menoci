<?php


interface EpicPidApiInterface {

  /**
   * @param \EpicPid $pid
   *
   * @return bool|mixed Newly created PID on success, FALSE otherwise
   * @throws \Exception
   */
  public function create(EpicPid $epicPid);

  public function testAuthentication(EpicPidService $epicPidService): bool;

  public function update(EpicPid $epicPid): bool;
}