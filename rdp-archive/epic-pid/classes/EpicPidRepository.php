<?php
/**
 * @file
 * ToDo: Short description for file.
 *
 * Long description for file.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class EpicPidRepository
 */
class EpicPidRepository {

  private static $tableName = 'epic_pid';

  private static $databaseFields = [
    'id',
    'service_id',
    'target_url',
    'status',
  ];

  /**
   * Translate database result to class instance.
   *
   * @param stdClass $result Generic database result object.
   *
   * @return EpicPid
   */
  private static function databaseResultToObject($result) {
    $epicPid = new EpicPid();

    if (empty($result)) {
      return $epicPid;
    }

    $epicPid->setId($result->id);
    $epicPid->setServiceId($result->service_id);
    $epicPid->setTargetUrl($result->target_url);
    $epicPid->setStatus($result->status);

    return $epicPid;
  }

  /**
   * Translate multiple database results to class instances.
   *
   * @param stdClass[] $results Array of generic database result objects.
   *
   * @return \EpicPid[] Array of EpicPid instances.
   */
  private static function databaseResultsToObjects($results) {
    $epicPids = [];
    foreach ($results as $result) {
      $epicPids[] = self::databaseResultToObject($result);
    }

    return $epicPids;
  }

  /**
   * ToDo: doc
   *
   * @param \EpicPid $epicPid
   *
   * @return bool|string
   */
  public static function save(EpicPid $epicPid) {

    $service = EpicPidServiceRepository::findById($epicPid->getServiceId());
    $api = $service->getAPI();

    // create or update a PID for this PID-Service
    if ($epicPid->isEmpty()) {
      // try to get new PID
      $id = $api->create($epicPid);
    }
    else {
      if ($api->update($epicPid)) {
        if ($service->getMethod() == EpicPidService::MANUAL) {
          // If service mode is manual generation, set PID status.
          $epicPid->setStatus(EpicPidStatus::ASSIGNED);
        }
      }
      $id = $epicPid->getId();
    }

    try {
      db_merge(self::$tableName)
        ->key(['id' => $id])
        ->fields([
            'service_id' => $epicPid->getServiceId(),
            'target_url' => $epicPid->getTargetUrl(),
            'status' => $epicPid->getStatus(),
          ]
        )->execute();

      return $id;

    } catch (Exception $e) {

      watchdog_exception('epic_pid', $e);
      drupal_set_message($e->getMessage(), 'error');

      return FALSE;
    }
  }

  /**
   * @param string $id
   *
   * @return \EpicPid
   */
  public static function findById($id) {

    $result = db_select(self::$tableName, 't')
      ->fields('t', self::$databaseFields)
      ->condition('id', $id, '=')
      ->range(0, 1)
      ->execute()
      ->fetch();

    return self::databaseResultToObject($result);
  }

  /**
   * @return \EpicPid[]
   */
  public static function findAllByServiceId($service_id) {
    $results = db_select(self::$tableName, 't')
      ->fields('t', self::$databaseFields)
      ->condition('service_id', $service_id, '=')
      ->execute()
      ->fetchAll();

    return self::databaseResultsToObjects($results);
  }

  /**
   * @return \EpicPid[]
   */
  public static function findAll() {
    $results = db_select(self::$tableName, 't')
      ->fields('t', self::$databaseFields)
      ->execute()
      ->fetchAll();

    return self::databaseResultsToObjects($results);
  }


}