<?php


class SurfsaraApi implements EpicPidApiInterface {

  private static $status;

  private $headers = [];

  private function init(EpicPidService $pidService) {
    $ch = curl_init();

    // Set the authentication options
    if (!$pidService->getAuthType() == EpicPidService::AUTH_TYPE_HANDLE_KEYS) {
      throw new \Exception("Only Key/Certificate based authentication supported for SURFsara API Client.");
    }

    /**
     * Handle.net Key/Certificate based authentication method
     *
     * @see https://userinfo.surfsara.nl/systems/epic-pid/usage/handle-http-json-rest-api-php
     */
    $certificateOnly = $pidService->getFilepathCertificate();
    $privateKey = $pidService->getFilepathPrivateKey();

    if (!file_exists($certificateOnly)) {
      throw new Exception('Missing cert: ' . $certificateOnly);
    }

    if (!file_exists($privateKey)) {
      throw new Exception('Missing key: ' . $privateKey);
    }

    curl_setopt($ch, CURLOPT_SSLCERT, $certificateOnly);
    curl_setopt($ch, CURLOPT_SSLKEY, $privateKey);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

    $this->headers[] = 'Authorization: Handle clientCert="true"';
    //curl_setopt($ch, CURLOPT_HTTPHEADER, ['Authorization: Handle clientCert="true"']);

    // Should cURL return or print out the data? (true = return, false = print)
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, TRUE);

    // Verbose for debugging
    curl_setopt($ch, CURLOPT_VERBOSE, TRUE);

    return $ch;
  }

  private function execute($ch) {

    curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
    $output = curl_exec($ch);
    $info = curl_getinfo($ch);

    // Download the given URL, and return output

    if ($info['http_code'] == 200) {
      self::$status = "HANDLE EXISTS";
    }
    if ($info['http_code'] == 201) {
      self::$status = "PID CREATED";
    }
    if ($info['http_code'] == 204) {
      self::$status = "PID UPDATED";
    }
    if ($info['http_code'] == 404) {
      self::$status = "HANDLE DOESNT EXIST";
    }

    curl_close($ch);

    // ToDo: Error handling here

    return $output;
  }

  /**
   * @param \EpicPid $pid
   *
   * @return bool|mixed Newly created PID on success, FALSE otherwise
   * @throws \Exception
   */
  public function create(EpicPid $epicPid) {
    throw new \Exception("Method not implemented for SURFsara API client.");
  }

  public function testAuthentication(EpicPidService $epicPidService): bool {
    $ch = $this->init($epicPidService);

    $url = $epicPidService->getUrl()
      . "?prefix=" . $epicPidService->getServicePrefix()
      . "&page=1&pageSize=0";
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_NOBODY, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);

    $result = curl_exec($ch);
    $info = curl_getinfo($ch);

    if ($info['http_code'] == 200) {
      return TRUE;
    }
    if ($info['http_code'] == 401) {
      return FALSE;
    }
  }

  public function update(EpicPid $epicPid): bool {
    $pidService = EpicPidServiceRepository::findById($epicPid->getServiceId());
    // Get cURL resource
    $ch = $this->init($pidService);

    // build the call URL (with full PID for update)
    $url = $pidService->getServiceUrl() . $epicPid->getId();
    curl_setopt($ch, CURLOPT_URL, $url);

    // set PUT Action
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');

    $data = [
      [
        "index" => 1,
        "type" => "URL",
        "data" => $epicPid->getTargetUrl(),
      ],
      [
        "index" => 100,
        "type" => "HS_ADMIN",
        "data" => [
          "value" => [
            "index" => 200,
            "handle" => "0.NA/" . $pidService->getServicePrefix(),
            "permissions" => "011111110011",
            "format" => "admin",
          ],
          "format" => "admin",
        ],
      ],
    ];

    $update_json = json_encode($data);

    //Set the headers to complete the request
    $this->headers[] = 'Content-Type: application/json';
    $this->headers[] = 'Content-Length: ' . strlen($update_json);

    //Set the post field data
    curl_setopt($ch, CURLOPT_POSTFIELDS, $update_json);

    $this->execute($ch);

    //drupal_set_message(self::$status);

    if (self::$status == "PID UPDATED" || self::$status == "PID CREATED") {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }
}