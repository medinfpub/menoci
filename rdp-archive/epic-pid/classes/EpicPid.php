<?php
/**
 * @file
 * ToDo: Short description for file.
 *
 * Long description for file.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class EpicPID
 *
 * ToDo: implement & doc
 */
class EpicPid implements PersistentIdentifierInterface {

  const PATH_EDIT = 'pid_edit';

  const FORM_EDIT = 'pid_form_edit';

  private $id = PersistentIdentifierInterface::EMPTY_ID;

  private $service_id;

  private $target_url;

  private $status = EpicPidStatus::UNMANAGED;

  /**
   * @return mixed A Persistent Identifier or constant EMPTY_ID if not yet
   *   stored.
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param int $id
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * @return int Internal ID of the PID-issuing service.
   */
  public function getServiceId() {
    return $this->service_id;
  }

  /**
   * @param mixed $service_id
   */
  public function setServiceId($service_id) {
    $this->service_id = $service_id;
  }

  /**
   * @return string The URL the Persistent Identifier points to.
   */
  public function getTargetUrl() {
    return $this->target_url;
  }

  /**
   * @param mixed $target_url
   */
  public function setTargetUrl($target_url) {
    $this->target_url = $target_url;
  }

  /**
   * Save identifier with the associated PID service.
   */
  public function save() {

    $id = EpicPidRepository::save($this);
    if ($this->isEmpty() && $id) {
      $this->setId($id);
    }
  }

  /**
   * @param string $size Icon size in pixels
   * (i.e. input of 12 results in 12px by 12px icon display)
   *
   * @return string HTML markup
   */
  private static function getIconHtml($size = '12') {
    $path = drupal_get_path("module", "epic_pid") . '/resources/';
    $iconpath = $path . 'epic_icon_16x16.png';
    $variables = [
      'path' => $iconpath,
      'width' => $size . 'px',
      'height' => $size . 'px',
      'alt' => 'EPIC PID',
      'attributes' => [],
    ];
    return theme_image($variables);
  }

  /**
   * @param string Icon size in pixels
   * (i.e. input of 12 results in 12px by 12px icon display)
   *
   * @return string HTML markup with link to resolver
   */
  public function getIconHtmlLink($size = '12') {
    $icon = self::getIconHtml($size);
    $link = $this->getUrl();
    return l($icon, $link, ['html' => TRUE]);
  }

  /**
   * @return string a JSON encoded string representing the PID instance
   */
  public function json() {
    return json_encode([
      $this->id,
      $this->service_id,
      $this->target_url,
      EpicPidStatus::translate($this->status),
    ]);
  }

  /**
   * @return string The PID with service-prefix but without resolver URL.
   */
  public function getPid() {
    $service = EpicPidServiceRepository::findById($this->service_id);
    return $service->getServicePrefix() . '/' . $this->id;

  }

  /**
   * @return string The resolve-able URL representation of this PID.
   */
  public function getUrl() {
    $service = EpicPidServiceRepository::findById($this->service_id);
    $url = $service->getResolveUrl() . $service->getServicePrefix() . '/' . $this->id;
    return $url;
  }

  /**
   * @return bool True if empty, i.e. instance has not yet been stored.
   */
  public function isEmpty() {
    if ($this->id == self::EMPTY_ID) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * @return mixed
   */
  public function getStatus() {
    return $this->status;
  }

  /**
   * @param mixed $status
   */
  public function setStatus($status) {
    $this->status = $status;
  }

  public function path($type = self::PATH_EDIT) {
    switch ($type) {
      case self::PATH_EDIT:
        return str_replace('%', $this->getId(), EPIC_PID_URL_CONFIG_PID_EDIT);
    }
  }

  public function getForm($type = self::FORM_EDIT) {
    $form = [];
    $form['target_url'] = [
      '#type' => 'textfield',
      '#title' => t('Target URL'),
      '#default_value' => $this->target_url,
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Save',
    ];

    $form['cancel'] = [
      '#type' => 'button',
      '#submit' => ['epic_pid_edit_cancel'],
      '#value' => t('Cancel'),
      '#executes_submit_callback' => TRUE,
      '#limit_validation_errors' => [],
    ];

    return $form;
  }
}