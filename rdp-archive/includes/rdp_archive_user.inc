<?php
/**
 * @file
 * ToDo: Short description for file.
 *
 * Long description for file.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

function rdp_archive_user($user_id) {

  // get logged-in user
  global $user;

  // get the user which's page is to be displayed
  $user_profile = UsersRepository::findByUid($user_id);

  // if the logged-in user is not the page owner deny access
  if ($user_profile->getUid() !== $user->uid) {
    drupal_access_denied();
  }

  $output = '';

  /************************* PERSONAL ARCHIVES ********************************/
  if (user_access(RDP_ARCHIVE_PERMISSION_PERSONAL_ARCHIVE)) {
    $archives_list = '';
    $archives = ArchiveRepository::findAllPersonalArchivesByUser($user_profile->getUid());
    /**
     * ToDo?: create personal archive for user on first visit if user has permission
     */


    foreach ($archives as $archive) {
      $archives_list .= '<p>' . l($archive->getName(), $archive->url()) . '</p>';
    }

    // if any personal archives exist, display them
    if (!empty($archives_list)) {
      $output .= '<h2>Personal Archives</h2>';
      $output .= $archives_list;
    }

  }

  /************************* GROUP ARCHIVES ***********************************/
  if (user_access(RDP_ARCHIVE_PERMISSION_GROUP_ARCHIVE)) {
    // fetch user's WorkingGroups
    $groups = $user_profile->getUserWorkingGroups();

    $archives_list = "";
    foreach ($groups as $group) {
      // List Archives for this WorkingGroup
      $archives = ArchiveRepository::findAllByGroupId($group->getId(), Archive::ARCHIVE_ENABLED);
      foreach ($archives as $archive) {
        $archives_list .= '<p>' . l($archive->getName(), $archive->url()) . ' (' .
          $group->getName() . ')</p>';
      }
    }

    // if any group archives exist, display them
    if (!empty($archives_list)) {
      $output .= '<h2>Group Archives</h2>';
      $output .= $archives_list;
    }

  }

  /************************* NO ARCHIVES AVAILABLE ****************************/
  if (empty($output)) {
    $output .= '<div class="panel panel-info">
                <div class="panel-body">
                <p class="lead">
                <span class="glyphicon glyphicon-info-sign text-info"></span>
                &nbsp;' .
      t("There are currently no Data Archives available for you.") .
      '</p></div></div>';
  }
  return $output;
}