<?php
/**
 * @file
 * Short description for file.
 *
 * Long description for file.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Drupal form to specify Archive roll out process
 */
function rdp_archive_rollout($form, $form_state) {

  // ToDo: implement multi-modal form

  // Select personal vs group archive

  // Select CDSTAR service
  // Optional: Select PID Sercive
  // Optional: select user role
  // Optional: activate/enable archives?

  // Activate Archives?

  $form = [];

  $form['select_mode'] = [
    '#type' => 'select',
    '#title' => 'Select roll-out mode',
    '#options' => [
      'personal' => 'Personal Archives (for all registered users)',
    ],
    '#default_value' => NULL,
    '#required' => TRUE,
  ];

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => 'Execute',
  ];

  return $form;
}


function rdp_archive_rollout_submit($form, &$form_state) {
  if (isset($form_state['values']['select_mode'])) {
    if ($form_state['values']['select_mode'] == 'personal') {
      rdp_archive_rollout_personal();
      $form_state['redirect'] = RDP_ARCHIVE_URL_CONFIG_OVERVIEW;
    }
  }
}

/**
 * Create Archive for every user
 */
function rdp_archive_rollout_personal() {

  $users = UsersRepository::findAll(); // Get all users

  foreach ($users as $user) {
    // Only create a new Personal Archive, if none exists.
    if (!ArchiveRepository::userHasPersonalArchive($user->getUid())) {
      ArchiveRepository::newPersonalArchive($user);
    }
  }
}