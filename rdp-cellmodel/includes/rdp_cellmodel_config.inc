<?php

/**
 * @file
 * Provides functions to configure aspects of the Cell Model Catalogue.
 * @author  Christoph Lehmann (christoph.lehmann@med.uni.goettingen.de)
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Provide configuration page for cell model catalogue. This page is meant to
 * be accessible by user with the RDP_CELLMODEL_PERMISSION_MANAGE permission
 * level.
 *
 * @throws \Exception
 */
function rdp_cellmodel_config($form, &$form_state) {
  $form = [];

  // Checkbox for enabling the module front box.
  $form[RDP_CELLMODEL_CONFIG_API_KEY] = [
    '#type' => 'textfield',
    '#title' => t('CRC Biobank API Key'),
    #'#description' => t('insert description.'),
    '#default_value' => variable_get(RDP_CELLMODEL_CONFIG_API_KEY, ""),
  ];

  // API button
  $form['back'] = [
    '#type' => 'button',
    '#submit' => ['rdp_cellmodel_config_api'],
    '#value' => '<i class="	glyphicon glyphicon-transfer"></i> ' . t('API'),
    '#executes_submit_callback' => TRUE,
    '#limit_validation_errors' => [],
    '#attributes' => ['class' => ['btn-primary btn-sm']],
  ];

  // Checkbox for enabling the module front box.
  $form[RDP_CELLMODEL_CONFIG_TITLE_IMAGE] = [
    '#type' => 'checkbox',
    '#title' => t('display CMC module image'),
    '#description' => t('If enabled the module image for CMC module will be displayed for authorized users.'),
    '#default_value' => variable_get(RDP_CELLMODEL_CONFIG_TITLE_IMAGE, RDP_CELLMODEL_CONFIG_TITLE_IMAGE_DEFAULT),
  ];

  // Checkbox for marking the primary underlying Cell Line in a darker tone.
  $form[RDP_CELLMODEL_CONFIG_DARK_PRIMARY_SAME_DONOR_LINES] = [
    '#type' => 'checkbox',
    '#title' => t('darker primary same donor line'),
    '#description' => t('If enabled display the primary cell line of the
    same donor/underlying cell line in a darker color than the rest.'),
    '#default_value' => variable_get(RDP_CELLMODEL_CONFIG_DARK_PRIMARY_SAME_DONOR_LINES,
      RDP_CELLMODEL_CONFIG_DARK_PRIMARY_SAME_DONOR_LINES_DEFAULT),
  ];

  // Checkbox for enabling registering private cell lines to hPSCreg.
  $form[RDP_CELLMODEL_CONFIG_REGISTER_PRIVATE] = [
    '#type' => 'checkbox',
    '#title' => t('allow registering of private cell lines'),
    '#description' => t('If enabled the "Register at hPSCreg" buttons will s
    how up even if the Cell Line Visibility is set to private. A warning will be shown in the register window anyhow.'),
    '#default_value' => variable_get(RDP_CELLMODEL_CONFIG_REGISTER_PRIVATE,
      RDP_CELLMODEL_CONFIG_REGISTER_PRIVATE_DEFAULT),
  ];

  // Checkbox for enabling registering private cell lines to hPSCreg.
  $form[RDP_CELLMODEL_CONFIG_SHORT_CLONE_TABLE_HEADER] = [
    '#type' => 'checkbox',
    '#title' => t('use short header titles for clone table'),
    '#description' => t('If enabled the titles in the clone window will be shortened e.g. "Pluripotency PCR" will become "Plur. PCR".'),
    '#default_value' => variable_get(RDP_CELLMODEL_CONFIG_SHORT_CLONE_TABLE_HEADER,
      RDP_CELLMODEL_CONFIG_SHORT_CLONE_TABLE_HEADER_DEFAULT),
  ];

    // Table for managing all the ethics presets. Managers can edit and add presets here.
    // Fieldset for the disease table.
    $form['fieldset-manage-sourcetypes'] = [
        '#type' => 'fieldset',
        '#title' => t('Manage Source Types'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
    ];

    $sourcetypes = arrayValueToKey(CellLineSourceRepository::findAll());

    $table_header = [
        'Name of Source Type',
        '',
    ];

    // Generate all the rows of the table.
    $table_rows = [];
    foreach ($sourcetypes as $sourcetype) {

        // Generate the clickable "Edit" button for this disease.
        $btn_edit = l('<span class="glyphicon glyphicon-pencil"></span> Delete',
            rdp_cellmodel_url(RDP_CELLMODEL_URL_CONFIG_NEW_SOURCETYPE) . '?action=delete&s=' . $sourcetype, [
                'html' => TRUE,
                'attributes' => [
                    'class' => ['btn btn-default btn-xs'],
                ],
            ]);

        // Each row represents a disease with ICD code, Disease name and clickable "Edit" button.
        $table_rows[] = [$sourcetype, $btn_edit];
    }

    // Assemble the whole table.
    $table = theme('table', [
        'header' => $table_header,
        'rows' => $table_rows,
    ]);

    $content = $table;

    // Put the table into a form inside the fieldset defined before and make some space to the next element.
    $form['fieldset-manage-sourcetypes']['manage-sourcetypes'] = [
        '#markup' => $content . '<div>&nbsp;<div>',
    ];

    // Button for adding a new disease.
    $form['fieldset-manage-sourcetypes']['new-sourcetype'] = [
        '#type' => 'submit',
        '#value' => 'New Source Type',
        '#attributes' => ['class' => ['btn-success btn-sm']],
        '#submit' => ['rdp_cellmodel_config_submit_sourcetype'],
    ];

  // Table for managing all the ethics presets. Managers can edit and add presets here.
  // Fieldset for the disease table.
  $form['fieldset-manage-presets'] = [
    '#type' => 'fieldset',
    '#title' => t('Manage Ethics Presets'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  ];

  // Define the header for the ethics preset table. The empty '' column makes space for the edit buttons.
  $preset_objects = CellLinePresetRepository::findAll();
  $table_header = [
    'Preset Name',
    '',
  ];

  // Generate all the rows of the table.
  $table_rows = [];
  foreach ($preset_objects as $preset_object) {

    // Generate the clickable "Edit" button for this disease.
    $btn_edit = l('<span class="glyphicon glyphicon-pencil"></span> Edit',
      rdp_cellmodel_url(RDP_CELLMODEL_URL_CONFIG_NEW_PRESET) . '?id=' . $preset_object->getId(), [
        'html' => TRUE,
        'attributes' => [
          'class' => ['btn btn-default btn-xs'],
        ],
      ]);

    // Each row represents a disease with ICD code, Disease name and clickable "Edit" button.
    $table_rows[] = [$preset_object->getName(), $btn_edit];
  }

  // Assemble the whole table.
  $table = theme('table', [
    'header' => $table_header,
    'rows' => $table_rows,
  ]);

  if (empty($preset_objects)) {
    $content = 'There are no Ethics Presets.';
  }
  else {
    $content = $table;
  }

  // Put the table into a form inside the fieldset defined before and make some space to the next element.
  $form['fieldset-manage-presets']['manage-presets'] = [
    '#markup' => $content . '<div>&nbsp;<div>',
  ];

  // Button for adding a new disease.
  $form['fieldset-manage-presets']['new-preset'] = [
    '#type' => 'submit',
    '#value' => '<span class="glyphicon glyphicon-plus"></span> New Preset',
    '#attributes' => ['class' => ['btn-success btn-sm']],
    '#submit' => ['rdp_cellmodel_config_submit_preset'],
  ];

  $sourcetypes = CellLineSourceRepository::findAll();
    if (empty($sourcetypes)){
        foreach (SOURCETYPES_DEFAULT as $sourcetype){
            drupal_set_message($sourcetype);
            CellLineSourceRepository::save($sourcetype);
        }
  }
  $sourcetypes = arrayValueToKey(CellLineSourceRepository::findAll());

  // Table for managing all the diseases. Managers can edit and add diseases here.
  // Fieldset for the disease table.
  $form['fieldset-manage-diseases'] = [
    '#type' => 'fieldset',
    '#title' => t('Manage Diseases'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  ];

  // Define the header for the disease table. The empty '' column makes space for the edit buttons.
  $table_header = [
    'ICD',
    'Disease',
    '',
  ];

  // Gather all the diseases and arrange them in an array.
  $diseases_objects = CellLineDiseaseRepository::findAll();
  $diseases = [];
  foreach ($diseases_objects as $disease_object) {
    $diseases[] = [
      'icd' => $disease_object->getIcd(),
      'disease_name' => $disease_object->getDiseaseName(),
      'id' => $disease_object->getId(),
    ];
  }

  // Sort the diseases alphabetically by disease name located in $diseases['disease_name'].
  usort($diseases, function ($a, $b) {
    return strcmp($a['disease_name'], $b['disease_name']);
  });

  // Generate all the rows of the table.
  $table_rows = [];
  foreach ($diseases as $disease) {

    // Generate the clickable "Edit" button for this disease.
    $btn_edit = l('<span class="glyphicon glyphicon-pencil"></span> Edit',
      rdp_cellmodel_url(RDP_CELLMODEL_URL_CONFIG_NEW_DISEASE) . '?id=' . $disease['id'], [
        'html' => TRUE,
        'attributes' => [
          'class' => ['btn btn-default btn-xs'],
        ],
      ]);

    // Each row represents a disease with ICD code, Disease name and clickable "Edit" button.
    $table_rows[] = [$disease['icd'], $disease['disease_name'], $btn_edit];
  }

  // Assemble the whole table.
  $table = theme('table', [
    'header' => $table_header,
    'rows' => $table_rows,
  ]);

  // Put the table into a form inside the fieldset defined before and make some space to the next element.
  $form['fieldset-manage-diseases']['manage-diseases'] = [
    '#markup' => $table . '<div>&nbsp;<div>',
  ];

  // Button for adding a new disease.
  $form['fieldset-manage-diseases']['new-disease'] = [
    '#type' => 'submit',
    '#value' => '<span class="glyphicon glyphicon-plus"></span> New Disease',
    '#attributes' => ['class' => ['btn-success btn-sm']],
    '#submit' => ['rdp_cellmodel_config_submit_disease'],
  ];

  // Fieldset for the visibility settings for external users.
  $form['fieldset-visibility-external'] = [
    '#type' => 'fieldset',
    '#title' => t('Visibility for External Users'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  ];

  // Put the checkboxes into the fieldset. The checkboxes are based on the FieldTitles array.
  // This arrays primary use is to store all the titles of the fields when editing or viewing
  // cell lines at one place but it is used here to manage the visibility aswell since it
  // is always connected to the shown fields on e.g. the view page of a cell line.
  $form['fieldset-visibility-external'][RDP_CELLMODEL_CONFIG_VISIBILITY_EXTERNAL] = [
    '#type' => 'checkboxes',
    '#options' => CellmodelConstants::getFieldTitles(),
    '#title' => t('checked box = the field is visible for external users'),
    '#default_value' => variable_get(RDP_CELLMODEL_CONFIG_VISIBILITY_EXTERNAL, RDP_CELLMODEL_DEFAULT_SETTINGS_VISIBILITY),
  ];



  // This automatically generates a "Save configuration" button which saves all
  // the values in the forms in variables depending on the form name.
  return system_settings_form($form);
}

/**
 * Submit function for the "New Disease" button.
 *
 * @see \rdp_cellmodel_config_disease()
 */
function rdp_cellmodel_config_submit_disease($form, &$form_state) {
  // Just redirect the user to the "New Disease" form.
  $form_state['redirect'] = rdp_cellmodel_url(RDP_CELLMODEL_URL_CONFIG_NEW_DISEASE);
}

/**
 * Submit function for the "New Preset" button.
 *
 * @see \rdp_cellmodel_config_preset()
 */
function rdp_cellmodel_config_submit_preset($form, &$form_state) {
  // Just redirect the user to the "New Preset" form.
  $form_state['redirect'] = rdp_cellmodel_url(RDP_CELLMODEL_URL_CONFIG_NEW_PRESET);
}

function rdp_cellmodel_config_submit_sourcetype($form, &$form_state){
    $form_state['redirect'] = rdp_cellmodel_url(RDP_CELLMODEL_URL_CONFIG_NEW_SOURCETYPE).'?action=add';
}

/**
 * Submit function for the back button.
 */
function rdp_cellmodel_config_api($form, &$form_state) {

  // Redirect back to the Edit Cell Line form.
  $form_state['redirect'] = rdp_cellmodel_url(RDP_CELLMODEL_URL_PROJECTS_JSON_API) . '?api_key=' . variable_get(RDP_CELLMODEL_CONFIG_API_KEY, '');
}
