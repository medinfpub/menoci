<?php

/**
 * @file
 * Provide functions to search for CellLines in CellLineRepository. Used by
 *   select2 field.
 *
 * @author  Christoph Lehmann (christoph.lehmann@med.uni.goettingen.de)
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Act as a router to the correct function depending on the provided URL.
 *
 * @return string|null
 */
function rdp_cellmodel_api() {

  $call = arg(2);
  $parameter = arg(3);

  if ($call) {
    switch ($call) {
      case 'search-cell-line':
        return rdp_cellmodel_api_cell_line($parameter);
      default:
        return 'Error: Unknown API call.';
    }
  }
  else {
    drupal_not_found();
    exit();
  }
}

/**
 * Search for CellLines. Used by Select2 fields when linking CellLines with
 * publications.
 *
 * @param string $string
 *   Search string (= the string in the current select2 field).
 *
 * @return null
 */
function rdp_cellmodel_api_cell_line($string) {
  $matches = [];

  // Only search if a search string is given. This is the string entered in the select2 field.
  if ($string) {
    // Assemble the database conditions to search for Cell Lines.
    $search_patterns = db_or();
    $search_patterns->condition('id', $string, '=');
    $string = "%$string%";
    $search_patterns->condition('hpscreg_name', $string, 'LIKE');
    $search_patterns->condition('lab_id', $string, 'LIKE');
    $search_patterns->condition('ipsc_id', $string, 'LIKE');
    $search_patterns->condition('alt_id', $string, 'LIKE');
    $search_patterns->condition('cell_type', $string, 'LIKE');

    // Search for Cell Lines in the database.
    $cell_lines = CellLineRepository::findByPublic($search_patterns);

    // Generate matches applicable by the select2 field.
    $matches = rdp_cellmodel_api_search_cell_line_create_matches_array($cell_lines);
  }

  // Return the results in JSON.
  drupal_json_output($matches);
  return NULL;
}

/**
 * Create an array with the cell lines from rdp_cellmodel_api_cell_line.
 *
 * @param \CellLine[] $cell_lines
 *
 * @return array
 *   The array with search matches.
 * @see rdp_cellmodel_api_cell_line.
 */
function rdp_cellmodel_api_search_cell_line_create_matches_array($cell_lines) {
  $matches = [];

  // Generate an array applicable by the select2 field. It contains all the values which
  // are shown in the search results of the select2 field.
  foreach ($cell_lines as $cell_line) {
    // Search for the primary diagnosis of the current value so its ICD and disease can be added.
    $diagnosis = CellLineDiagnosisRepository::findByCellLineIdAndPrimary($cell_line->getId());
    $name = empty($hspreg_name = $cell_line->getHpscregName()) ? $cell_line->getIpscId() : $hspreg_name;
    $matches[] = [
      'hpscreg_name' => check_plain($name),
      'id' => check_plain($cell_line->getId()),
      'cell_type' => check_plain($cell_line->getCellType()),
      'icd' => check_plain($diagnosis->getIcd()),
      'disease' => check_plain($diagnosis->getDisease()),
      'source' => check_plain($cell_line->getSourceType()),
      'reprogramming' => check_plain($cell_line->getReprogrammingMethod()),
      'grade' => check_plain($cell_line->getGrade()),
    ];
  }
  return $matches;
}
