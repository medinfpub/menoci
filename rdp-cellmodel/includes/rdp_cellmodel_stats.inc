<?php

/**
 * @file
 * Provide the forms and functions for importing cell line data.
 * @author  Christoph Lehmann (christoph.lehmann@med.uni.goettingen.de)
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Show statistics about the cell model catalogue.
 */
function rdp_cellmodel_stats() {

  // Print the tab buttons.
  $tabs_form = drupal_get_form('rdp_cellmodel_form_tabs_stats');
  $html = drupal_render($tabs_form);

  // Add the diagrams/logs.
  $html .= rdp_cellmodel_rdp_statistics();

  return $html;
}
