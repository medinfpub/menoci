<?php

/**
 * @file
 * Provide database layer for @see \CellLineStarLimsClone.
 *
 * @author  Christoph Lehmann (christoph.lehmann@med.uni.goettingen.de)
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */
class CellLineStarLimsCloneRepository
{

  // ------------------------ <<< STATIC VARIABLES >>> -------------------------

  /**
   * @var string
   *   Name of the database table containing CellLineStarLimsClone.
   */
  static $tableName = 'cellmodel_cell_line_starlims_clone';

  /**
   * @var array
   *   All database fields for CellLineStarLimsClone.
   */
  static $databaseFields = [
    'id',
    'cell_line',
    'starlims_id',
    'starlims_lab_id',
    'tubes',
  ];

  // -------------------------- <<< SAVE & DELETE >>> --------------------------

  /**
   * Store CellLineStarLimsClone into the database.
   *
   * @param \CellLineStarLimsClone $starlims_clone
   *   CellLineStarLimsClone object to be stored.
   *
   * @throws \InvalidMergeQueryException
   */
  public static function save($starlims_clone) {
    db_merge(self::$tableName)
      ->key(['id' => $starlims_clone->getId()])
      ->fields([
        'cell_line' => $starlims_clone->getCellLine(),
        'starlims_id' => $starlims_clone->getCloneStarlimsId(),
        'starlims_lab_id' => $starlims_clone->getCloneStarlimsLabId(),
        'tubes' => $starlims_clone->getCloneStarlimsTubes(),
      ])
      ->execute();
  }

  // ----------------------- <<< RESULT TO OBJECT(S) >>> -----------------------

  /**
   * Read database result and create a new CellLineStarLimsClone object.
   *
   * @param $result
   *  Database results of a finder function.
   *
   * @return \CellLineStarLimsClone
   *   New CellLineStarLimsClone object.
   */
  public static function databaseResultsToStarlimsClone($result) {
    $starlims_clone = new CellLineStarLimsClone();

    if (empty($result)) {
      return NULL;
    }

    // Set the variables.
    $starlims_clone->setId($result->id);
    $starlims_clone->setCellLine($result->cell_line);
    $starlims_clone->setCloneStarlimsId($result->starlims_id);
    $starlims_clone->setCloneStarlimsLabId($result->starlims_lab_id);
    $starlims_clone->setCloneStarlimsTubes($result->tubes);

    return $starlims_clone;
  }

  /**
   * Read database results and create an array with CellLineStarLimsClone objects.
   *
   * @param $results
   *  Database results of a finder function.
   *
   * @return \CellLineStarLimsClone[]
   *   New CellLineStarLimsClone objects.
   */
  public static function databaseResultsToStarlimsClones($results) {
    $starlims_clones = [];
    foreach ($results as $result) {
      $starlims_clones[] = self::databaseResultsToStarlimsClone($result);
    }
    return $starlims_clones;
  }

  // ------------------------- <<< FINDER FUNCTIONS >>> ------------------------

  /**
   * Find all CellLineStarLimsClones.
   *
   * @return \CellLineStarLimsClone[]
   *   Found CellLineStarLimsClone objects.
   */
  public static function findAll($header = []) {
    $result = db_select(self::$tableName, 'a')
      ->fields('a', self::$databaseFields)
      ->extend('TableSort')
      ->orderByHeader($header)
      ->execute();

    return self::databaseResultsToStarlimsClones($result);
  }

  /**
   * Find the CellLineStarLimsClone with a specific database ID.
   *
   * @param $id
   *   The ID of the given CellLineStarLimsClone.
   *
   * @return \CellLineStarLimsClone
   *   Found CellLineStarLimsClone object.
   */
  public static function findById($id) {
    $result = db_select(self::$tableName, 'a')
      ->condition('id', $id, '=')
      ->fields('a', self::$databaseFields)
      ->execute()
      ->fetch();

    return self::databaseResultsToStarlimsClone($result);
  }

  /**
   * Find the CellLineStarLimsClone with a specific starlims ID.
   *
   * @param $starlims_id
   *   The StarLIMS id of the given CellLineStarLimsClone.
   *
   * @return \CellLineStarLimsClone
   *   Found CellLineStarLimsClone object.
   */
  public static function findByStarlimsId($starlims_id) {
    $result = db_select(self::$tableName, 'a')
      ->condition('starlims_id', $starlims_id, '=')
      ->fields('a', self::$databaseFields)
      ->execute()
      ->fetch();

    return self::databaseResultsToStarlimsClone($result);
  }

  /**
   * Find the CellLineStarLimsClone with a specific Lab ID.
   *
   * @param $starlims_id
   *   The Lab id  of the given CellLineStarLimsClone.
   *
   * @return \CellLineStarLimsClone
   *   Found CellLineStarLimsClone object.
   */
  public static function findByLabId($lab_id) {
    $result = db_select(self::$tableName, 'a')
      ->condition('starlims_lab_id', $lab_id, '=')
      ->fields('a', self::$databaseFields)
      ->execute()
      ->fetch();

    return self::databaseResultsToStarlimsClone($result);
  }

  /**
   * Return CellLineStarLimsClone of given CellLine (Database ID).
   *
   * @param int $cell_line_id
   *   The ID of the given CellLine.
   *
   * @return \CellLineStarLimsClone[]
   *  Found CellLineStarLimsClone objects.
   */
  public static function findByCellLineId($cell_line_id, $header = []) {
    $result = db_select(self::$tableName, 't')
      ->condition('cell_line', $cell_line_id, '=')
      ->fields('t', self::$databaseFields)
      ->extend('TableSort')
      ->orderByHeader($header)
      ->execute();

    return self::databaseResultsToStarlimsClones($result);
  }
}
