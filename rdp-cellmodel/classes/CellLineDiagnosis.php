<?php

/**
 * @file
 * Provide class for the CellLineDiagnosis objects. Provide form fields
 * for each variable when building @see \rdp_cellmodel_cell_line_new_edit().
 *
 * @author  Christoph Lehmann (christoph.lehmann@med.uni.goettingen.de)
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class CellLineDiagnosis
 */
class CellLineDiagnosis {

  // --------------------------- <<< VARIABLES >>> -----------------------------

  /**
   * @var int
   *   The database ID of the diagnosis.
   */
  private $id;

  /**
   * @var int
   *   The respective cell line.
   */
  private $cell_line;

  /**
   * @var string
   *   The name of the disease.
   */
  private $disease;

  /**
   * @var string
   *   The ICD of the disease.
   */
  private $icd;

  /**
   * @var string
   *   Primary or Secondary Disease?
   */
  private $type;

  // ----------------------- <<< GETTERS & SETTERS >>> -------------------------

  /**
   * @return int $id
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param int $id
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * @return int $cell_line
   */
  public function getCellLine() {
    return $this->cell_line;
  }

  /**
   * @param int $cell_line
   */
  public function setCellLine($cell_line) {
    $this->cell_line = $cell_line;
  }

  /**
   * @return string $type
   */
  public function getType() {
    return $this->type;
  }

  /**
   * @param string $type
   */
  public function setType($type) {
    $this->type = $type;
  }

  /**
   * @return string $disease
   */
  public function getDisease() {
    if (user_access(RDP_CELLMODEL_PERMISSION_MANAGE) or
      user_access(RDP_CELLMODEL_PERMISSION_VIEW) or
      variable_get(RDP_CELLMODEL_CONFIG_VISIBILITY_EXTERNAL)['disease_name']) {
      return $this->disease;

    }
    else {
      return '';
    }
  }

  /**
   * @param string $disease
   */
  public function setDisease($disease) {
    $this->disease = $disease;
  }

  /**
   * @return string $icd
   */
  public function getIcd() {
    if (user_access(RDP_CELLMODEL_PERMISSION_MANAGE) or
      user_access(RDP_CELLMODEL_PERMISSION_VIEW) or
      variable_get(RDP_CELLMODEL_CONFIG_VISIBILITY_EXTERNAL)['icd']) {
      return $this->icd;

    }
    else {
      return '';
    }
  }

  /**
   * @param string $icd
   */
  public function setIcd($icd) {
    $this->icd = $icd;
  }

  // ---------------------------<<< FORM FIELDS >>>-----------------------------

  /**
   * @param int $count
   *   Current count of diseases.
   *
   * @return array
   *   Disease fieldset.
   */
  public function getFormFieldDiagFieldset($count) {
    // The first diagnosis is always the primary diagnosis. All others are
    // secondary.
    switch ($count) {
      case 0:
        $title_prefix = 'Primary';
        break;
      default:
        $title_prefix = generate_counting_string($count) . ' Secondary';
    }
    return [
      '#type' => 'fieldset',
      '#title' => $title_prefix . ' Diagnosis',
    ];
  }

  /**
   * @return array
   *   Disease form field.
   */
  public function getFormFieldDisease() {
    $options = CellmodelConstants::getOptionsDiseaseNames();
    asort($options);
    return [
      '#type' => 'select',
      '#title' => t('Disease'),
      '#empty_value' => '',
      '#options' => $options,
      '#default_value' => $this->getDisease(),
      '#ajax' => [
        'callback' => 'ajax_callback',
        'wrapper' => 'diagnosis-fieldset-wrapper',
        'event' => 'change',
        'effect' => 'fade',
      ],
    ];
  }

  /**
   * @return array
   *   ICD form field.
   */
  public function getFormFieldIcd() {
    // Generate the correct URL to the specific ICD on https://www.icd-code.de/.
    if (!empty($this->getIcd())) {
      $icd = $this->getIcd();
      $replace = substr($icd, 0, strpos($icd, ".")) . '.-';
      $url = URL_ICD_LOOKUP;
      $url = preg_replace('/%1/', $replace, $url, 1);
      $url = preg_replace('/%2/', $icd, $url, 1);
    }
    else {
      $url = URL_ICD;
    }

    return [
      '#type' => 'textfield',
      '#title' => t('ICD Code <a href= ' . $url . ' target="_blank"><i class="glyphicon glyphicon-question-sign">
    </i></a>'),
      '#description' => t('<i>ICD</i> = Code of the International Statistical
     Classification of Diseases and Related Health Problems'),
      '#default_value' => $this->getIcd(),
      '#attributes' => [
        'placeholder' => t('- will be automatically assigned -'),
        'readonly' => 'readonly',
      ],
    ];
  }

  // -------------------------<<< OTHER FUNCTIONS >>>---------------------------

  /**
   * Saves the data of this CellLineDiagnosis into the database.
   */
  public function save() {
    try {
      CellLineDiagnosisRepository::save($this);
    } catch (InvalidMergeQueryException $exception) {
      drupal_set_message($exception, 'error');
    }
  }
}
