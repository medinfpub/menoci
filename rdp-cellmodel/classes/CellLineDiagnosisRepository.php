<?php

/**
 * @file
 * Provide database layer for @see CellLineDiagnosis.
 *
 * @author  Christoph Lehmann (christoph.lehmann@med.uni.goettingen.de)
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class CellLineDiagnosisRepository
 */
class CellLineDiagnosisRepository {

  // ------------------------ <<< STATIC VARIABLES >>> -------------------------

  /**
   * @var string
   *   Name of the database table containing CellLineDiagnosis.
   */
  static $tableName = 'cellmodel_cell_line_diagnosis';

  /**
   * @var array
   *   All database fields for CellLineDiagnosis.
   */
  static $databaseFields = [
    'id',
    'cell_line',
    'type',
    'disease',
    'icd',
  ];

  // -------------------------- <<< SAVE & DELETE >>> --------------------------

  /**
   * Store CellLineDiagnosis into database.
   *
   * @param \CellLineDiagnosis $diagnosis
   *   CellLineDiagnosis object to be saved.
   *
   * @throws \InvalidMergeQueryException
   */
  public static function save($diagnosis) {
    db_merge(self::$tableName)
      ->key(['id' => $diagnosis->getId()])
      ->fields([
        'cell_line' => $diagnosis->getCellLine(),
        'type' => $diagnosis->getType(),
        'disease' => $diagnosis->getDisease(),
        'icd' => $diagnosis->getIcd(),
      ])
      ->execute();
  }

  /**
   * Remove a CellLineDiagnosis entry from the database.
   *
   * @param int $diagnosis_id
   *   ID of the respective CellLineDiagnosis to be deleted.
   */
  public static function delete($diagnosis_id) {
    db_delete(self::$tableName)
      ->condition('id', $diagnosis_id, '=')
      ->execute();
  }

  // ----------------------- <<< RESULT TO OBJECT(S) >>> -----------------------

  /**
   * Read database result and create a new CellLineDiagnosis object.
   *
   * @param \stdClass $result
   *   Database result of a finder function.
   *
   * @return \CellLineDiagnosis
   *   New CellLineDiagnosis object.
   */
  public static function databaseResultsToDiagnosis($result) {
    $diagnosis = new CellLineDiagnosis();

    if (empty($result)) {
      return $diagnosis;
    }

    // Set the variables.
    $diagnosis->setId($result->id);
    $diagnosis->setType($result->type);
    $diagnosis->setCellLine($result->cell_line);
    $diagnosis->setDisease($result->disease);
    $diagnosis->setIcd($result->icd);

    return $diagnosis;
  }

  /**
   * Read database results and create an array with CellLineDiagnosis objects.
   *
   * @param \DatabaseStatementInterface $results
   *   Database results of a finder function.
   *
   * @return \CellLineDiagnosis[]
   *   New CellLineDiagnosis objects.
   */
  public static function databaseResultsToDiagnoses($results) {
    $diagnoses = [];
    foreach ($results as $result) {
      $diagnoses[] = self::databaseResultsToDiagnosis($result);
    }

    return $diagnoses;
  }

  // ------------------------- <<< FINDER FUNCTIONS >>> ------------------------

  /**
   * Return CellLineDiagnoses which match the given condition.
   *
   * @param $condition
   *   The applied database condition.
   *
   * @return \CellLineDiagnosis[]
   *   Found CellLineDiagnosis objects.
   */
  public static function findBy($condition) {
    $results = db_select(self::$tableName, 't')
      ->condition($condition)
      ->fields('t', self::$databaseFields)
      ->execute();

    return self::databaseResultsToDiagnoses($results);
  }

  /**
   * Return CellLineDiagnosis of given CellLine (Database ID).
   *
   * @param int $cell_line
   *   ID of the given CellLine.
   *
   * @return \CellLineDiagnosis[]
   *   Found CellLineDiagnosis object.
   */
  public static function findByCellLineId($cell_line) {
    $result = db_select(self::$tableName, 'a')
      ->condition('cell_line', $cell_line, '=')
      ->fields('a', self::$databaseFields)
      ->execute();

    return self::databaseResultsToDiagnoses($result);
  }

  /**
   * Return PRIMARY CellLineDiagnosis of given CellLine (Database ID).
   *
   * @param int $cell_line
   *   ID of the given CellLine.
   *
   * @return \CellLineDiagnosis
   *   Found CellLineDiagnosis object.
   */
  public static function findByCellLineIdAndPrimary($cell_line) {
    $result = db_select(self::$tableName, 'a')
      ->condition('cell_line', $cell_line, '=')
      ->condition('type', 'primary', '=')
      ->fields('a', self::$databaseFields)
      ->range(0, 1)
      ->execute()
      ->fetch();

    return self::databaseResultsToDiagnosis($result);
  }

  /**
   * Return SECONDARY CellLineDiagnosis of given CellLine (Database ID).
   *
   * @param int $cell_line
   *   ID of the given CellLine.
   *
   * @return \CellLineDiagnosis[]
   *   Found CellLineDiagnosis object.
   */
  public static function findByCellLineIdAndSecondary($cell_line) {
    $result = db_select(self::$tableName, 'a')
      ->condition('cell_line', $cell_line, '=')
      ->condition('type', 'secondary', '=')
      ->fields('a', self::$databaseFields)
      ->execute();

    return self::databaseResultsToDiagnoses($result);
  }
}
