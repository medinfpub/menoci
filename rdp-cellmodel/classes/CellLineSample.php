<?php

/**
 * @file
 * Provide class for the CellLineSample objects.
 *
 * @author  Christoph Lehmann (christoph.lehmann@med.uni.goettingen.de)
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class CellLineSample
 */
class CellLineSample {

  // --------------------------- <<< VARIABLES >>> -----------------------------

  /**
   * @var int
   *   Database ID.
   */
  private $id;

  /**
   * @var string
   *   Lab ID of cell line.
   */
  private $cell_line;

  /**
   * @var string
   *   Type of sample.
   */
  private $type_sample;

  /**
   * @var string
   *   Type of primary container.
   */
  private $type_primary_container;

  /**
   * @var string
   *   Recentrifugation.
   */
  private $precentrifugation;

  /**
   * @var string
   *   First centrifugation.
   */
  private $centrifugation1;

  /**
   * @var string
   *   Second centrifugation.
   */
  private $centrifugation2;

  /**
   * @var string
   *   Postcentrifugation.
   */
  private $postcentrifugation;

  /**
   * @var string
   *   Long term storage.
   */
  private $long_term_storage;

  /**
   * @var string
   *   Amount of sample left (in mL).
   */
  private $amount_left;

  /**
   * @var string
   *   Container External ID.
   */
  private $container_external_id;

  /**
   * @var int
   *   Container inventory ID.
   */
  private $container_inventory_id;

  /**
   * @var int
   *   Container position X.
   */
  private $container_pos_x;

  /**
   * @var int
   *   Container position Y.
   */
  private $container_pos_y;

  /**
   * @var int
   *   Internal inventory ID.
   */
  private $internal_inventory_id;

  /**
   * @var string
   *   Kit ID.
   */
  private $kit_id;

  /**
   * @var string
   *   Location.
   */
  private $location;

  /**
   * @var string
   *   Material code.
   */
  private $material_code;

  /**
   * @var string
   *   Project name.
   */
  private $project_name;

  /**
   * @var string
   *   Sample container.
   */
  private $sample_container;

  /**
   * @var string
   *   Sample ID.
   */
  private $sample_id;

  /**
   * @var int
   *   Tube barcode.
   */
  private $tube_barcode;

  // ----------------------- <<< GETTERS & SETTERS >>> -------------------------

  /**
   * @return int
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param int $id
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * @return int
   */
  public function getCellLine() {
    return $this->cell_line;
  }

  /**
   * @param int $cell_line
   */
  public function setCellLine($cell_line) {
    $this->cell_line = $cell_line;
  }

  /**
   * @return string
   */
  public function getTypeSample() {
    return $this->type_sample;
  }

  /**
   * @param string $type_sample
   */
  public function setTypeSample($type_sample) {
    $this->type_sample = $type_sample;
  }

  /**
   * @return string
   */
  public function getTypePrimaryContainer() {
    return $this->type_primary_container;
  }

  /**
   * @param string $type_primary_container
   */
  public function setTypePrimaryContainer($type_primary_container) {
    $this->type_primary_container = $type_primary_container;
  }

  /**
   * @return string
   */
  public function getPrecentrifugation() {
    return $this->precentrifugation;
  }

  /**
   * @param string $precentrifugation
   */
  public function setPrecentrifugation($precentrifugation) {
    $this->precentrifugation = $precentrifugation;
  }

  /**
   * @return string
   */
  public function getCentrifugation1() {
    return $this->centrifugation1;
  }

  /**
   * @param string $centrifugation1
   */
  public function setCentrifugation1($centrifugation1) {
    $this->centrifugation1 = $centrifugation1;
  }

  /**
   * @return string
   */
  public function getCentrifugation2() {
    return $this->centrifugation2;
  }

  /**
   * @param string $centrifugation2
   */
  public function setCentrifugation2($centrifugation2) {
    $this->centrifugation2 = $centrifugation2;
  }

  /**
   * @return string
   */
  public function getPostcentrifugation() {
    return $this->postcentrifugation;
  }

  /**
   * @param string $postcentrifugation
   */
  public function setPostcentrifugation($postcentrifugation) {
    $this->postcentrifugation = $postcentrifugation;
  }

  /**
   * @return string
   */
  public function getLongTermStorage() {
    return $this->long_term_storage;
  }

  /**
   * @param string $long_term_storage
   */
  public function setLongTermStorage($long_term_storage) {
    $this->long_term_storage = $long_term_storage;
  }

  /**
   * @return string
   */
  public function getAmountLeft() {
    return $this->amount_left;
  }

  /**
   * @param string $amount_left
   */
  public function setAmountLeft($amount_left) {
    $this->amount_left = $amount_left;
  }

  /**
   * @return string
   */
  public function getContainerExternalId() {
    return $this->container_external_id;
  }

  /**
   * @param string $container_external_id
   */
  public function setContainerExternalId($container_external_id) {
    $this->container_external_id = $container_external_id;
  }

  /**
   * @return int
   */
  public function getContainerInventoryId() {
    return $this->container_inventory_id;
  }

  /**
   * @param int $container_inventory_id
   */
  public function setContainerInventoryId($container_inventory_id) {
    $this->container_inventory_id = $container_inventory_id;
  }

  /**
   * @return int
   */
  public function getContainerPosX() {
    return $this->container_pos_x;
  }

  /**
   * @param int $container_pos_x
   */
  public function setContainerPosX($container_pos_x) {
    $this->container_pos_x = $container_pos_x;
  }

  /**
   * @return int
   */
  public function getContainerPosY() {
    return $this->container_pos_y;
  }

  /**
   * @param int $container_pos_y
   */
  public function setContainerPosY($container_pos_y) {
    $this->container_pos_y = $container_pos_y;
  }

  /**
   * @return int
   */
  public function getInternalInventoryId() {
    return $this->internal_inventory_id;
  }

  /**
   * @param int $internal_inventory_id
   */
  public function setInternalInventoryId($internal_inventory_id) {
    $this->internal_inventory_id = $internal_inventory_id;
  }

  /**
   * @return string
   */
  public function getKitId() {
    return $this->kit_id;
  }

  /**
   * @param string $kit_id
   */
  public function setKitId($kit_id) {
    $this->kit_id = $kit_id;
  }

  /**
   * @return string
   */
  public function getLocation() {
    return $this->location;
  }

  /**
   * @param string $location
   */
  public function setLocation($location) {
    $this->location = $location;
  }

  /**
   * @return string
   */
  public function getMaterialCode() {
    return $this->material_code;
  }

  /**
   * @param string $material_code
   */
  public function setMaterialCode($material_code) {
    $this->material_code = $material_code;
  }

  /**
   * @return string
   */
  public function getProjectName() {
    return $this->project_name;
  }

  /**
   * @param string $project_name
   */
  public function setProjectName($project_name) {
    $this->project_name = $project_name;
  }

  /**
   * @return string
   */
  public function getSampleContainer() {
    return $this->sample_container;
  }

  /**
   * @param string $sample_container
   */
  public function setSampleContainer($sample_container) {
    $this->sample_container = $sample_container;
  }

  /**
   * @return string
   */
  public function getSampleId() {
    return $this->sample_id;
  }

  /**
   * @param string $sample_id
   */
  public function setSampleId($sample_id) {
    $this->sample_id = $sample_id;
  }

  /**
   * @return int
   */
  public function getTubeBarcode() {
    return $this->tube_barcode;
  }

  /**
   * @param int $tube_barcode
   */
  public function setTubeBarcode($tube_barcode) {
    $this->tube_barcode = $tube_barcode;
  }

  // -------------------------<<< OTHER FUNCTIONS >>>---------------------------

  /**
   * Saves the data of this CellLineSample into the database.
   */
  public function save() {
    try {
      CellLineSampleRepository::save($this);
    } catch (InvalidMergeQueryException $exception) {
      drupal_set_message($exception, 'error');
    }
  }
}
