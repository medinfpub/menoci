<?php

/**
 * @file
 * Provide class for the CellLineReprogrammingDate objects. Provide form fields
 * for each variable when building @see \rdp_cellmodel_cell_line_new_edit().
 *
 * @author  Christoph Lehmann (christoph.lehmann@med.uni.goettingen.de)
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class CellLineReprogrammingDate
 */
class CellLineReprogrammingDate {

  // --------------------------- <<< VARIABLES >>> -----------------------------

  /**
   * @var int
   *   The database id of the rep. date.
   */
  private $id;

  /**
   * @var int
   *   The ID of the respective cell line.
   */
  private $cell_line;

  /**
   * @var string
   *   The reprogramming date.
   */
  private $date;

  // ----------------------- <<< GETTERS & SETTERS >>> -------------------------

  /**
   * @return int $id
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param int $id
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * @return int $cell_line
   */
  public function getCellLine() {
    return $this->cell_line;
  }

  /**
   * @param int $cell_line
   */
  public function setCellLine($cell_line) {
    $this->cell_line = $cell_line;
  }

  /**
   * @return string $date
   */
  public function getDate() {
    return $this->date;
  }

  /**
   * @param string $date
   */
  public function setDate($date) {
    $this->date = $date;
  }

  // ---------------------------<<< FORM FIELDS >>>-----------------------------

  /**
   * @param $count int
   *   The current count of rep. dates.
   *
   * @return array
   *   Reprogramming Date form field.
   */
  public function getFormFieldRepDate($count) {
    $count++;
    $title_prefix = generate_counting_string($count);

    return [
      '#date_label_position' => 'none',
      '#type' => 'date_popup',
      '#name' => 'RepDate',
      '#default_value' => $this->getDate(),
      '#date_format' => RDP_CELLMODEL_DEFAULT_DATE_FORMAT,
      '#description' => t('The date at which the reprogramming took place.'),
      '#prefix' => '<p style="font-size:9pt;"> ' . $title_prefix . ' ' . CellmodelConstants::getFieldTitles()['reprogramming_date'] . '</p>',
      '#suffix' => '<div>&nbsp;</div>',
      '#attributes' => [
        'autocomplete' => 'off',
      ],
    ];
  }

  // -------------------------<<< OTHER FUNCTIONS >>>---------------------------

  /**
   * Saves the data of this CellLineReprogrammingDate into the database.
   */
  public function save() {
    try {
      CellLineReprogrammingDateRepository::save($this);
    } catch (InvalidMergeQueryException $exception) {
      drupal_set_message($exception, 'error');
    }

  }
}
