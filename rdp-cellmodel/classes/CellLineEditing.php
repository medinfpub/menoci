<?php

/**
 * @file
 * Provide class for the CellLineEditing objects. Provide form fields
 * for each variable when building @see \rdp_cellmodel_cell_line_new_edit().
 *
 * @author  Christoph Lehmann (christoph.lehmann@med.uni.goettingen.de)
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class CellLineEditing
 */
class CellLineEditing {

  // --------------------------- <<< VARIABLES >>> -----------------------------

  /**
   * @var int
   *   Database ID of the editing.
   */
  private $id;

  /**
   * @var int
   *   ID of the respective cell line.
   */
  private $cell_line;

  /**
   * @var string
   *   Used clones.
   */
  private $clone_used;

  /**
   * @var string
   */
  private $editing_strategy;

  /**
   * @var string
   */
  private $editing_targeted_gene;

  /**
   * @var string
   */
  private $visibility_targeted_gene;

  /**
   * @var string
   */
  private $editing_method1;

  /**
   * @var string
   */
  private $editing_method2;

  /**
   * @var string
   */
  private $editing_method3;

  /**
   * @var array
   */
  private $editing_dates = [];

  // ----------------------- <<< GETTERS & SETTERS >>> -------------------------

  /**
   * @return string
   */
  public function getCloneUsed() {
    return $this->clone_used;
  }

  /**
   * @param string $clone_used
   */
  public function setCloneUsed($clone_used) {
    $this->clone_used = $clone_used;
  }

  /**
   * @return string
   */
  public function getEditingStrategy() {
    if (user_access(RDP_CELLMODEL_PERMISSION_MANAGE) or
      user_access(RDP_CELLMODEL_PERMISSION_VIEW) or
      variable_get(RDP_CELLMODEL_CONFIG_VISIBILITY_EXTERNAL)['editing_strategy']) {
      return $this->editing_strategy;

    }
    else {
      return '';
    }
  }

  /**
   * @param string $editing_strategy
   */
  public function setEditingStrategy($editing_strategy) {
    $this->editing_strategy = $editing_strategy;
  }

  /**
   * @return string
   */
  public function getEditingTargetedGene() {
    if (user_access(RDP_CELLMODEL_PERMISSION_MANAGE) or
      user_access(RDP_CELLMODEL_PERMISSION_VIEW) or
      $this->getEditingTargetedGeneVisibility() == 'Public') {
      return $this->editing_targeted_gene;
    }
    else {
      if (empty($this->editing_targeted_gene)) {
        return '';
      }
      else {
        return '<i>unreleased</i>';
      }
    }
  }

  /**
   * @param string $editing_targeted_gene
   */
  public function setEditingTargetedGene($editing_targeted_gene) {
    $this->editing_targeted_gene = $editing_targeted_gene;
  }

  /**
   * @return string
   */
  public function getEditingMethod1() {
    return $this->editing_method1;
  }

  /**
   * @param string $editing_method1
   */
  public function setEditingMethod1($editing_method1) {
    $this->editing_method1 = $editing_method1;
  }

  /**
   * @return string
   */
  public function getEditingMethod2() {
    return $this->editing_method2;
  }

  /**
   * @param string $editing_method2
   */
  public function setEditingMethod2($editing_method2) {
    $this->editing_method2 = $editing_method2;
  }

  /**
   * @return string
   */
  public function getEditingMethod3() {
    return $this->editing_method3;
  }

  /**
   * @param string $editing_method3
   */
  public function setEditingMethod3($editing_method3) {
    $this->editing_method3 = $editing_method3;
  }

  /**
   * @return \CellLineEditingDate[]
   */
  public function getEditingDates() {
    return $this->editing_dates;
  }

  /**
   * @param array $editing_dates
   */
  public function setEditingDates($editing_dates) {
    $this->editing_dates = $editing_dates;
  }

  /**
   * @return int
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param int $id
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * @return int
   */
  public function getCellLine() {
    return $this->cell_line;
  }

  /**
   * @param int $cell_line
   */
  public function setCellLine($cell_line) {
    $this->cell_line = $cell_line;
  }

  /**
   * @return string
   */
  public function getEditingTargetedGeneVisibility() {
    return $this->visibility_targeted_gene;
  }

  /**
   * @param string $editing_targeted_gene_visibility
   */
  public function setEditingTargetedGeneVisibility($editing_targeted_gene_visibility) {
    $this->visibility_targeted_gene = $editing_targeted_gene_visibility;
  }

  // ---------------------------<<< FORM FIELDS >>>-----------------------------

  /**
   * @return array
   */
  public function getFormFieldCloneUsed($genetic_editing) {
    if ($genetic_editing) {
      $type = 'textfield';
    }
    else {
      $type = 'hidden';
    }

    return [
      '#type' => $type,
      '#title' => CellmodelConstants::getFieldTitles()['clone_used'],
      '#default_value' => $this->clone_used,
      '#attributes' => ['autocomplete' => 'off',],
    ];
  }

  /**
   * @return array
   */
  function getFormFieldEditingStrategy() {
    return [
      '#type' => 'select',
      '#title' => CellmodelConstants::getFieldTitles()['editing_strategy'],
      '#default_value' => $this->editing_strategy,
      '#empty_value' => '',
      '#options' => CellmodelConstants::getOptionsEditingStrategy(),
    ];
  }

  /**
   * @return array
   */
  public function getFormFieldTargetedGene() {
    return [
      '#type' => 'textfield',
      '#title' => CellmodelConstants::getFieldTitles()['editing_targeted_gene'],
      '#default_value' => $this->editing_targeted_gene,
      '#attributes' => [
        'placeholder' => t('e.g. KRAS'),
      ],
    ];
  }

  /**
   * @return array
   *   Visibility of Targeted Gene form field.
   */
  public function getFormFieldVisibilityTargetedGene() {
    return [
      '#type' => 'radios',
      '#title' => CellmodelConstants::getFieldTitles()['visibility_targeted_gene'] . ' *',
      '#default_value' => $this->visibility_targeted_gene,
      '#empty_value' => '',
      '#options' => CellmodelConstants::getOptionsVisibility(),
      '#states' => [
        'visible' => [
          ':input[name*="ed_targeted_gene"]' => [
            'filled' =>
              TRUE,
          ],
        ],
      ],
    ];
  }

  /**
   * @return array
   */
  function getFormFieldEditingMethod1() {
    return [
      '#type' => 'select',
      '#default_value' => $this->editing_method1,
      '#empty_value' => '',
      '#options' => CellmodelConstants::getOptionsEditingMethod1(),
      '#prefix' => '
    <div class="inline">
      <div class="col-xs-4">',
      '#suffix' => '</div>
    </div>',
    ];
  }

  /**
   * @return array
   */
  function getFormFieldEditingMethod2() {
    return [
      '#type' => 'select',
      '#default_value' => $this->editing_method2,
      '#empty_value' => '',
      '#options' => CellmodelConstants::getOptionsEditingMethod2(),
      '#prefix' => '
    <div class="inline">
      <div class="col-xs-4">',
      '#suffix' => '</div>
    </div>',
    ];
  }

  /**
   * @return array
   */
  function getFormFieldEditingMethod3() {
    return [
      '#type' => 'select',
      '#default_value' => $this->editing_method3,
      '#empty_value' => '',
      '#options' => CellmodelConstants::getOptionsEditingMethod3(),
      '#prefix' => '
    <div class="row">
      <div class="col-xs-4">',
      '#suffix' => '</div>
    </div>',
    ];
  }

  /**
   * Button for adding one more editing date.
   *
   * @return array
   *   Add Editing Date button.
   */
  public function getFormFieldEditDateAdd() {
    return [
      '#type' => 'submit',
      '#value' => t('Add'),
      '#name' => 'AddEditDateButton',
      '#submit' => ['ajax_submit'],
      '#ajax' => [
        'callback' => 'ajax_callback',
        'wrapper' => 'editing-fieldset-wrapper',
        'effect' => 'fade',
      ],
      '#limit_validation_errors' => [],
      '#attributes' => ['class' => ['btn-primary btn-sm']],
    ];
  }

  /**
   * Button for removing one editing date.
   *
   * @return array
   *   Remove Editing Date button.
   */
  public function getFormFieldEditDateRemove($i) {
    return [
      '#type' => 'submit',
      '#value' => t('Remove'),
      '#name' => 'RemoveEditDateButton' . $i,
      '#submit' => ['ajax_submit'],
      '#ajax' => [
        'callback' => 'ajax_callback',
        'wrapper' => 'editing-fieldset-wrapper',
        'effect' => 'fade',
      ],
      '#limit_validation_errors' => [],
      '#attributes' => [
        'class' => ['btn-primary btn-xs'],
        'style' => 'margin: 3px',
      ],
    ];
  }

  // -------------------------<<< OTHER FUNCTIONS >>>---------------------------

  /**
   * Saves the data of this CellLineEditing into the database.
   */
  public function save() {
    try {
      CellLineEditingRepository::save($this);
    } catch (InvalidMergeQueryException $exception) {
      drupal_set_message($exception, 'error');
    }
  }
}
