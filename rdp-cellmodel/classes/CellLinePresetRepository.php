<?php

/**
 * @file
 * Provide database layer for @see CellLinePreset.
 *
 * @author  Christoph Lehmann (christoph.lehmann@med.uni.goettingen.de)
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

class CellLinePresetRepository
{

  // ------------------------ <<< STATIC VARIABLES >>> -------------------------

  /**
   * @var string
   *   Name of the database table containing CellLinePreset.
   */
  static $tableName = 'cellmodel_cell_line_preset';

  /**
   * @var array
   *   All database fields for CellLinePreset.
   */
  static $databaseFields = [
    'id',
    'preset_name',
    'ethics_preset_1',
    'ethics_preset_2',
    'ethics_preset_3',
    'ethics_preset_4',
    'ethics_preset_5',
    'ethics_preset_6',
    'ethics_preset_7',
    'ethics_preset_8',
    'ethics_preset_9',
    'ethics_preset_10',
    'ethics_preset_11',
    'ethics_preset_12',
    'ethics_preset_13',
    'ethics_preset_14',
    'ethics_preset_15',
    'ethics_preset_16',
    'ethics_preset_17',
    'ethics_preset_18',
    'ethics_preset_19',
    'ethics_preset_20',
    'ethics_preset_21',
    'ethics_preset_22',
    'ethics_preset_23',
    'ethics_preset_24',
    'ethics_preset_25',
    'ethics_preset_26',
    'ethics_preset_27',
  ];

  // -------------------------- <<< SAVE & DELETE >>> --------------------------

  /**
   * Store CellLinePreset into the database.
   *
   * @param \CellLinePreset $preset
   *   CellLinePreset object to be stored.
   *
   * @throws \InvalidMergeQueryException
   */
  public static function save($preset) {
    db_merge(self::$tableName)
      ->key(['id' => $preset->getId()])
      ->fields([
        'preset_name' => $preset->getName(),
        'ethics_preset_1' => $preset->getEthicsPreset1(),
        'ethics_preset_2' => $preset->getEthicsPreset2(),
        'ethics_preset_3' => $preset->getEthicsPreset3(),
        'ethics_preset_4' => $preset->getEthicsPreset4(),
        'ethics_preset_5' => $preset->getEthicsPreset5(),
        'ethics_preset_6' => $preset->getEthicsPreset6(),
        'ethics_preset_7' => $preset->getEthicsPreset7(),
        'ethics_preset_8' => $preset->getEthicsPreset8(),
        'ethics_preset_9' => $preset->getEthicsPreset9(),
        'ethics_preset_10' => $preset->getEthicsPreset10(),
        'ethics_preset_11' => $preset->getEthicsPreset11(),
        'ethics_preset_12' => $preset->getEthicsPreset12(),
        'ethics_preset_13' => $preset->getEthicsPreset13(),
        'ethics_preset_14' => $preset->getEthicsPreset14(),
        'ethics_preset_15' => $preset->getEthicsPreset15(),
        'ethics_preset_16' => $preset->getEthicsPreset16(),
        'ethics_preset_17' => $preset->getEthicsPreset17(),
        'ethics_preset_18' => $preset->getEthicsPreset18(),
        'ethics_preset_19' => $preset->getEthicsPreset19(),
        'ethics_preset_20' => $preset->getEthicsPreset20(),
        'ethics_preset_21' => $preset->getEthicsPreset21(),
        'ethics_preset_22' => $preset->getEthicsPreset22(),
        'ethics_preset_23' => $preset->getEthicsPreset23(),
        'ethics_preset_24' => $preset->getEthicsPreset24(),
        'ethics_preset_25' => $preset->getEthicsPreset25(),
        'ethics_preset_26' => $preset->getEthicsPreset26(),
        'ethics_preset_27' => $preset->getEthicsPreset27(),
      ])
      ->execute();
  }

  // ----------------------- <<< RESULT TO OBJECT(S) >>> -----------------------

  /**
   * Read database result and create a new CellLinePreset object.
   *
   * @param $result
   *  Database results of a finder function.
   *
   * @return \CellLinePreset
   *   New CellLinePreset object.
   */
  public static function databaseResultsToPreset($result) {
    $preset = new CellLinePreset();

    if (empty($result)) {
      return $preset;
    }

    // Set the variables.
    $preset->setId($result->id);
    $preset->setName($result->preset_name);
    $preset->setEthicsPreset1($result->ethics_preset_1);
    $preset->setEthicsPreset2($result->ethics_preset_2);
    $preset->setEthicsPreset3($result->ethics_preset_3);
    $preset->setEthicsPreset4($result->ethics_preset_4);
    $preset->setEthicsPreset5($result->ethics_preset_5);
    $preset->setEthicsPreset6($result->ethics_preset_6);
    $preset->setEthicsPreset7($result->ethics_preset_7);
    $preset->setEthicsPreset8($result->ethics_preset_8);
    $preset->setEthicsPreset9($result->ethics_preset_9);
    $preset->setEthicsPreset10($result->ethics_preset_10);
    $preset->setEthicsPreset11($result->ethics_preset_11);
    $preset->setEthicsPreset12($result->ethics_preset_12);
    $preset->setEthicsPreset13($result->ethics_preset_13);
    $preset->setEthicsPreset14($result->ethics_preset_14);
    $preset->setEthicsPreset15($result->ethics_preset_15);
    $preset->setEthicsPreset16($result->ethics_preset_16);
    $preset->setEthicsPreset17($result->ethics_preset_17);
    $preset->setEthicsPreset18($result->ethics_preset_18);
    $preset->setEthicsPreset19($result->ethics_preset_19);
    $preset->setEthicsPreset20($result->ethics_preset_20);
    $preset->setEthicsPreset21($result->ethics_preset_21);
    $preset->setEthicsPreset22($result->ethics_preset_22);
    $preset->setEthicsPreset23($result->ethics_preset_23);
    $preset->setEthicsPreset24($result->ethics_preset_24);
    $preset->setEthicsPreset25($result->ethics_preset_25);
    $preset->setEthicsPreset26($result->ethics_preset_26);
    $preset->setEthicsPreset27($result->ethics_preset_27);

    return $preset;
  }

  /**
   * Read database results and create an array with CellLinePreset objects.
   *
   * @param $results
   *  Database results of a finder function.
   *
   * @return \CellLinePreset[]
   *   New CellLinePreset objects.
   */
  public static function databaseResultsToPresets($results) {
    $presets = [];
    foreach ($results as $result) {
      $presets[] = self::databaseResultsToPreset($result);
    }
    return $presets;
  }

  // ------------------------- <<< FINDER FUNCTIONS >>> ------------------------

  /**
   * Find all CellLinePreset.
   *
   * @return \CellLinePreset[]
   *   Found CellLinePreset objects.
   */
  public static function findAll() {
    $result = db_select(self::$tableName, 'a')
      ->fields('a', self::$databaseFields)
      ->execute();

    return self::databaseResultsToPresets($result);
  }

  /**
   * Find the CellLinePresets with a specific database ID.
   *
   * @param $id
   *   The ID of the given CellLinePreset.
   *
   * @return \CellLinePreset
   *   Found CellLinePreset object.
   */
  public static function findById($id) {
    $result = db_select(self::$tableName, 'a')
      ->condition('id', $id, '=')
      ->fields('a', self::$databaseFields)
      ->execute()
      ->fetch();

    return self::databaseResultsToPreset($result);
  }
}
