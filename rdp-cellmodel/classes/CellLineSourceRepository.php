<?php
class CellLineSourceRepository {

    static $tableName = 'cellmodel_cell_line_source';

    static $databaseFields = [
        'source_type',
    ];

    public static function save(string $source) {
        try {
            db_insert(self::$tableName)->fields(['source' => $source])->execute();
            return TRUE;
        }
        catch (Exception $e){
            return FALSE;
        }
    }

    public static function delete(string $source) {
        db_delete(self::$tableName)->condition('source', $source)->execute();
    }

    public static function findAll() {
        $result = db_select(self::$tableName, 's')->fields('s')->execute()->fetchAll(PDO::FETCH_COLUMN);
        $result = is_bool($result) ? array() : $result;
        return $result;
    }

}
