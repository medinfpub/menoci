<?php

/**
 * @file
 * Contains all constants and parameters for structured fields.
 *
 * @author  Christoph Lehmann (christoph.lehmann@med.uni.goettingen.de)
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 * SPDX-License-Identifier: GPL-3.0
 */

// ------------------------------<<< CONSTANTS >>>------------------------------
// Site URLs
define("RDP_CELLMODEL_URL_DEFAULT", 'cellmodel');
define("RDP_CELLMODEL_URL_CELL_LINE", RDP_CELLMODEL_URL_DEFAULT . '/cell-line');
define("RDP_CELLMODEL_URL_CELL_LINE_NEW_EDIT", RDP_CELLMODEL_URL_CELL_LINE . '/new-edit');
define("RDP_CELLMODEL_URL_CELL_LINE_VIEW", RDP_CELLMODEL_URL_CELL_LINE . '/view');
define("RDP_CELLMODEL_URL_CELL_LINE_HPSCREG", RDP_CELLMODEL_URL_CELL_LINE . '/hpscreg');
define("RDP_CELLMODEL_URL_DOWNLOAD", RDP_CELLMODEL_URL_DEFAULT . '/download');
define("RDP_CELLMODEL_URL_DOWNLOAD_CELL_LINES", RDP_CELLMODEL_URL_DOWNLOAD
  . '/cell-lines/');
define("RDP_CELLMODEL_URL_DOWNLOAD_TEMPLATE", RDP_CELLMODEL_URL_DOWNLOAD
  . '/template/');
define("RDP_CELLMODEL_URL_DOWNLOAD_CLONES", RDP_CELLMODEL_URL_DOWNLOAD . '/clones');
define("RDP_CELLMODEL_URL_UPLOAD", RDP_CELLMODEL_URL_DEFAULT . '/upload');
define("RDP_CELLMODEL_URL_UPLOAD_CELL_LINES", RDP_CELLMODEL_URL_UPLOAD
  . '/cell-lines/');
define('RDP_CELLMODEL_URL_CONFIG_DEFAULT', 'admin/config/rdp/' . RDP_CELLMODEL_URL_DEFAULT);
define('RDP_CELLMODEL_API', RDP_CELLMODEL_URL_DEFAULT . '/api');
define('RDP_CELLMODEL_API_SEARCH_CELL_LINE', RDP_CELLMODEL_API . '/search-cell-line');
define('RDP_CELLMODEL_URL_STATS', RDP_CELLMODEL_URL_DEFAULT . '/stats');
define('RDP_CELLMODEL_URL_CONFIG_NEW_DISEASE', RDP_CELLMODEL_URL_CONFIG_DEFAULT . '/disease');
define('RDP_CELLMODEL_URL_CONFIG_NEW_SOURCETYPE', RDP_CELLMODEL_URL_CONFIG_DEFAULT . '/sourcetype');
define('RDP_CELLMODEL_URL_CONFIG_NEW_PRESET', RDP_CELLMODEL_URL_CONFIG_DEFAULT . '/preset');
define('RDP_CELLMODEL_URL_CLONE_DETAILS', RDP_CELLMODEL_URL_DEFAULT . '/clones');
define('RDP_CELLMODEL_URL_STARLIMS_CLONE_DETAILS', RDP_CELLMODEL_URL_DEFAULT . '/clones_starlims');
define('RDP_CELLMODEL_URL_PROJECTS_JSON_API', RDP_CELLMODEL_URL_DEFAULT . '/projects.json');
define('RDP_CELLMODEL_URL_POST_TUBES_API', RDP_CELLMODEL_URL_DEFAULT . '/tubes');


// Permissions
define("RDP_CELLMODEL_PERMISSION_DEFAULT", 'rdp_cellmodel default');
define("RDP_CELLMODEL_PERMISSION_MANAGE", 'rdp_cellmodel manager');
define("RDP_CELLMODEL_PERMISSION_VIEW", 'rdp_cellmodel view');

// Paths
define("RDP_CELLMODEL_PATH_CELLMODEL_RESOURCES", $_SERVER['DOCUMENT_ROOT'] .
  base_path() . drupal_get_path('module', 'rdp_cellmodel') . '/resources');
define("RDP_CELLMODEL_PATH_EXCEL_TEMPLATE", RDP_CELLMODEL_PATH_CELLMODEL_RESOURCES . '/template_cell_lines.xlsx');
define("RDP_CELLMODEL_PATH_EXCEL_EXPORT", RDP_CELLMODEL_PATH_CELLMODEL_RESOURCES . '/export_cell_lines.xlsx');
define("RDP_CELLMODEL_PATH_EXCEL_EXPORT_CLONES", RDP_CELLMODEL_PATH_CELLMODEL_RESOURCES . '/export_clones.xlsx');

// Configuration
define('RDP_CMC_DRUPAL_MODULE_NAME', 'rdp_cellmodel');
define('RDP_CELLMODEL_DEFAULT_DATE_FORMAT', 'Y-m-d');
define('RDP_CELLMODEL_DEFAULT_LOG_DATE_FORMAT', 'Y-m-d H:i:s');
define('RDP_CELLMODEL_COLUMN_AUTO_SIZE_MAX', 100);
define('RDP_CELLMODEL_DEFAULT_STRING_MAX_LENGTH', 128);
define('RDP_CELLMODEL_TEMPLATE_FILE_VERSION_NUMBER', 6);
define('RDP_CELLMODEL_CONFIG_API_KEY', 'rdp_cellmodel_config_biobank_api_key');
define('RDP_CELLMODEL_TEMPLATE_FILE_NAME',
  'Cell_Line_Catalogue_Template_v' . RDP_CELLMODEL_TEMPLATE_FILE_VERSION_NUMBER . '.xlsx');
define('RDP_CELLMODEL_EXPORT_FILE_NAME',
  'Cell_Line_Catalogue_Export' . date(RDP_CELLMODEL_DEFAULT_DATE_FORMAT) . '.xlsx');
define('RDP_CELLMODEL_EXPORT_CLONES_FILE_NAME',
  'Cell_Line_Catalogue_Export_Clones' . date(RDP_CELLMODEL_DEFAULT_DATE_FORMAT) . '.xlsx');
define('RDP_CELLMODEL_DEFAULT_PROVIDER', [
  t('University Medical Center Goettingen'),
  'UMG',
]);

// Config Page
define("RDP_CELLMODEL_CONFIG_DARK_PRIMARY_SAME_DONOR_LINES", "rdp_cmc_config_dark_primary");
define("RDP_CELLMODEL_CONFIG_DARK_PRIMARY_SAME_DONOR_LINES_DEFAULT", TRUE);

define("RDP_CELLMODEL_CONFIG_TITLE_IMAGE", "rdp_cmc_config_title_image");
define("RDP_CELLMODEL_CONFIG_TITLE_IMAGE_DEFAULT", TRUE);

define("RDP_CELLMODEL_CONFIG_REGISTER_PRIVATE", "rdp_cmc_config_register_private");
define("RDP_CELLMODEL_CONFIG_REGISTER_PRIVATE_DEFAULT", TRUE);

define("RDP_CELLMODEL_CONFIG_SHORT_CLONE_TABLE_HEADER", "rdp_cmc_config_short_clone_header");
define("RDP_CELLMODEL_CONFIG_SHORT_CLONE_TABLE_HEADER_DEFAULT", TRUE);

define('RDP_CELLMODEL_CONFIG_VISIBILITY_EXTERNAL', 'rdp_cellmodel_config_visibility');
define('RDP_CELLMODEL_DEFAULT_SETTINGS_VISIBILITY', [
  'hpscreg_name' => 'hpscreg_name',
  'editing_id' => 'editing_id',
  'provider' => 'provider',
  'location' => 'location',
  'cell_type' => 'cell_type',
  'grade' => 'grade',
  'underlying_cell_line' => 'underlying_cell_line',
  'restric_areas' => 'restric_areas',
  'restric_research' => 'restric_research',
  'restric_commercial' => 'restric_commercial',
  'restric_clinical' => 'restric_clinical',
  'restric_additional' => 'restric_additional',
  'editing_strategy' => 'editing_strategy',
  'editing_targeted_gene' => 'editing_targeted_gene',
  'editing_method' => 'editing_method',
  'primary_diagnosis' => 'primary_diagnosis',
  'secondary_diagnosis' => 'secondary_diagnosis',
  'icd' => 'icd',
  'genetic_predisposition' => 'genetic_predisposition',
  'donor_gender' => 'donor_gender',
  'donor_race' => 'donor_race',
  'donor_age' => 'donor_age',
  'source_type' => 'source_type',
  'reprogramming_method' => 'reprogramming_method',
  'contact' => 'contact',
  'linked_publications' => 'linked_publications',
]);
define('DISEASE_ICD_DEFAULT', [
  ['I49.8', 'Brugada syndrome'],
  ['I47.2', 'Catecholaminergic polymorphic ventricular tachycardia'],
  ['I49.8', 'Long QT syndrome 1'],
  ['I49.8', 'Long QT syndrome 2'],
  ['I49.8', 'Long QT syndrome 3'],
  ['I49.8', 'Long QT syndrome 7'],
  ['I42.80', 'Arrhythmogenic right ventricular cardiomyopathy'],
  ['Q21.3', 'Tetralogy of Fallot'],
  ['I47.2', 'Ventricular tachycardia'],
  ['Q87.8', 'Vici syndrome'],
  ['I42.0', 'Dilated cardiomyopathy'],
  ['G71.0', 'Muscular dystrophy'],
  ['G71.2', 'X-linked myotubular myopathy'],
  ['I42.2', 'Hypertrophic cardiomyopathy'],
  ['I42.1', 'Hypertrophic obstructive cardiomyopathy'],
  ['E71.1', 'Barth syndrome'],
  ['Q23.4', 'Hypoplastic left heart syndrome'],
  ['L93.2', 'Neonatal lupus erythematosus'],
  ['F84.0', 'Autism'],
  ['G43.8', 'Alternating hemiplegia'],
  ['R00.1', 'Bradycardia'],
  ['I48.-', 'Atrial fibrillation'],
  ['Q87.1', 'Noonan syndrome'],
  ['Q87.8', 'Peroxisome biogenesis disorder spectrum'],
  ['E34.8', 'Progeroid syndrome'],
  ['F00.', 'Dementia in Alzheimer disease'],
  ['Q04.8', 'Periventricular nodular heterotopia'],
  ['G40.3', 'Generalised epilepsy with febrile seizure plus'],
  ['G35.0', 'Multiple sclerosis'],
  ['G71.3', 'Mitochondrial myopathy'],
  ['Q87.8', 'Alström syndrome'],
  ['Q87.1', 'Noonan syndrome with multiple lentigines'],
  ['Q87.1', 'Cardiofaciocutaneous syndrome'],
  ['F84.0', 'Childhood autism'],
  ['I49.8', 'Short QT syndrome'],
  ['I49.8', 'Overlap syndrome'],
  ['Q02', 'Microcephaly'],
  ['WT', 'WT'],
  ['WT', 'WT/Relative'],
  ['Unknown', 'Cardiomyopathy after chemotherapy'],
]);

define('SOURCETYPES_DEFAULT', [
    'Skin',
    'Blood',
    'Hair',
    'Fibroblasts',
    'Myoblasts',
    'Bone Marrow',
    'Gingiva',
    'Urine',
    
]);

// External URLs
define("URL_HPSCREG_PREVIEW", 'https://hpscreg.eu/api/cell-line/name_preview');
define("URL_HPSCREG_SUBMIT", 'https://hpscreg.eu/api/cell-line/name');
define("URL_HPSCREG_NAMING", 'https://hpscreg.eu/about/naming-tool');
define("URL_HPSCREG_LOOKUP", 'http://hescreg.eu/cell-line/%');
define("URL_ICD", 'http://www.icd-code.de/icd/code/ICD-10-GM.html');
define("URL_ICD_LOOKUP", 'http://www.icd-code.de/suche/icd/code/%1.html?sp=S%2');

class CellmodelConstants {

  /**
   * @return string[]
   */
  static function getOptionsYesNo() {
    return arrayValueToKey([
      'Yes',
      'No',
    ]);
  }

  /**
   * @return string[]
   */
  static function getOptionsYesNoNa() {
    return arrayValueToKey([
      CellmodelConstants::getOptionNa(),
      'Yes',
      'No',
    ]);
  }

  /**
   * @return string[]
   */
  static function getOptionsYesNoUnknown() {
    return arrayValueToKey([
      'Yes',
      'No',
      'Unknown',
    ]);
  }

  /**
   * @return string[]
   */
  static function getOptionsYesNoNotAskedNa() {
    return arrayValueToKey([
      CellmodelConstants::getOptionNa(),
      'Not asked',
      'Yes',
      'No',
    ]);
  }

  /**
   * @return string[]
   */
  static function getOptionsHdrNhej() {
    return arrayValueToKey([
      'HDR',
      'NHEJ',
    ]);
  }

  /**
   * @return string[]
   */
  static function getOptionsTabType() {
    return [
      'internal' => 'Patient-Derived Cell Lines',
      'gen_mod' => 'Genetically Modified Cell Lines',
      'external' => 'External Cell Lines',
      'as_project' => 'SFB1002 - AS Project',
    ];
  }

  /**
   * @return string[]
   */
  static function getOptionsLocation() {
    return arrayValueToKey([
      'Stem Cell Unit',
      'UMG Biobank',
    ]);
  }

  /**
   * @return string[]
   */
  static function getOptionsCellType() {
    return arrayValueToKey([
      'no sample',
      'only primary culture',
      'iPSC',
    ]);
  }

  /**
   * @return string[]
   */
  static function getOptionsGrade() {
    return arrayValueToKey([
      'Research',
      'GMP',
      'Other',
    ]);
  }

  /**
   * @return string[]
   */
  static function getOptionsKnownUnknown() {
    return arrayValueToKey([
      'Known',
      'Unknown',
    ]);
  }

  /**
   * @return string[]
   */
  static function getOptionsKnownUnknownNa() {
    return arrayValueToKey([
      CellmodelConstants::getOptionNa(),
      'Known',
      'Unknown',
    ]);
  }

  /**
   * @return string[]
   */
  static function getOptionsGender() {
    return arrayValueToKey([
      'Male',
      'Female',
      'Unknown',
    ]);
  }

  /**
   * @return string[]
   */
  static function getOptionsGenderNa() {
    return arrayValueToKey([
      CellmodelConstants::getOptionNa(),
      'Male',
      'Female',
      'Unknown',
    ]);
  }

  /**
   * @return string[]
   */
  static function getOptionsAge() {
    return arrayValueToKey([
      '0-10',
      '10-19',
      '20-29',
      '30-39',
      '40-49',
      '50-59',
      '60-69',
      '70-79',
      '<= 80',
    ]);
  }

  /**
   * @return string[]
   */
  static function getOptionsRace() {
    return arrayValueToKey([
      'Caucasian',
      'Mongoloid',
      'Negroid',
      'Unknown',
    ]);
  }

  /**
   * @return string[]
   */
  static function getOptionsSource() {
    return arrayValueToKey(CellLineSourceRepository::findAll());
  }

  /**
   * @return string[]
   */
  static function getOptionsRepMethod() {
    return arrayValueToKey([
      'Sendai Virus',
      'Lentivirus',
      'Retrovirus',
      'mRNA',
      'Plasmid',
      'Unknown',
    ]);
  }

  /**
   * @return string[]
   */
  static function getOptionsMedium() {
    return arrayValueToKey([
      'StemMACS™ iPS-Brew',
      'StemFlex™',
      'Essential 8™',
      'Other',
    ]);
  }

  /**
   * @return string[]
   */
  static function getOptionsCharacterization() {
    return arrayValueToKey([
      'Completed',
      'In Progress',
      'Not Performed',
    ]);
  }

  /**
   * @return string[]
   */
  static function getOptionsTesting() {
    return arrayValueToKey([
      'Tested',
      'Not Tested',
    ]);
  }

  /**
   * @return string[]
   */
  static function getOptionsTestingResults() {
    return arrayValueToKey([
      'Positive',
      'Negative',
    ]);
  }

  /**
   * @return string[]
   */
  static function getOptionsTestingResultsNa() {
    return arrayValueToKey([
      CellmodelConstants::getOptionNa(),
      'Positive',
      'Negative',
    ]);
  }

  /**
   * @return string[]
   */
  static function getOptionsTestingMaterial() {
    return arrayValueToKey([
      'Serum',
      'Cell Culture',
    ]);
  }

  /**
   * @return string[]
   */
  static function getOptionsTestingMaterialNa() {
    return arrayValueToKey([
      CellmodelConstants::getOptionNa(),
      'Serum',
      'Cell Culture',
    ]);
  }

  /**
   * @return string[]
   */
  static function getOptionsEthicsAnonym() {
    return arrayValueToKey([
      CellmodelConstants::getOptionNa(),
      'Pseudonymised',
      'Anonymised',
    ]);
  }

  /**
   * @return string[]
   */
  static function getOptionsEthicsAccess() {
    return arrayValueToKey([
      CellmodelConstants::getOptionNa(),
      'Open Access',
      'Controlled Access',
      'No Information',
    ]);
  }

  /**
   * @return string[]
   */
  static function getOptionsEthicsAnonymization() {
    return arrayValueToKey([
      CellmodelConstants::getOptionNa(),
      'Not asked',
      'Yes, after 15 years',
      'Yes, after 5 years',
      'No',
    ]);
  }

  /**
   * @return string[]
   */
  static function getOptionsEthicsTerminate() {
    return arrayValueToKey([
      CellmodelConstants::getOptionNa(),
      'Not asked',
      'Yes, the donated tissue will be destroyed in this case',
      'Yes, the derived products and data will be anonymized in this case',
      'Yes, the donated tissue will be destroyed and derived products and data will be anonymized in this case',
      'Yes, the donated tissue cannot be used in this case',
      'No',
    ]);
  }

  /**
   * @return string[]
   */
  static function getOptionsRelativeType() {
    return arrayValueToKey([
      'Father',
      'Mother',
      'Brother',
      'Sister',
      'Son',
      'Daughter',
      'Grandmother',
      'Grandfather',
      'Grandson',
      'Granddaughter',
      'Uncle',
      'Aunt',
      'Cousin',
      'Nephew',
      'Niece',
      'Other',
      'Unknown',
    ]);
  }

  /**
   * @return string[]
   */
  static function getOptionsVisibility() {
    return arrayValueToKey([
      'Public',
      'Private',
    ]);
  }

  /**
   * @return string[]
   */
  static function getOptionsDistribution() {
    return arrayValueToKey([
      'Project',
      'UMG Collection Strategy',
    ]);
  }

  /**
   * @return string[]
   */
  static function getOptionsEditingStrategy() {
    return arrayValueToKey([
      'knockout',
      'knockin',
      'SNP correction',
      'SNP introduction',
    ]);
  }

  /**
   * @return string[]
   */
  static function getOptionsEditingMethod1() {
    return arrayValueToKey([
      'TALEN',
      'CRISPR',
      'random',
    ]);
  }

  /**
   * @return string[]
   */
  static function getOptionsEditingMethod2() {
    return arrayValueToKey([
      'protein',
      'plasmid',
    ]);
  }

  static function getOptionsEditingMethod3() {
    return arrayValueToKey([
      'nucleofection',
      'lipofection',
    ]);
  }

  /**
   * @return string[]
   */
  static function getOptionsGenomicModification() {
    return arrayValueToKey([
      'KO',
      'KI',
      'SNP introduced',
      'SNP corrected',
      'not edited',
    ]);
  }

  /**
   * @return string[]
   */
  static function getOptionsZygosity() {
    return arrayValueToKey([
      'homozygous',
      'heterozygous',
      'compound heterozygous',
      'hemizygous',
    ]);
  }

  /**
   * @return string[]
   */
  static function getOptionsBiosafety($gen_mod = FALSE) {
    if ($gen_mod) {
      return [
        CellmodelConstants::getOptionNa(),
        'S1' => 'GVO S1',
        'S2' => 'GVO S2',
      ];
    } else {
      return [
        CellmodelConstants::getOptionNa(),
        'S1' => 'GVO S1',
        'S2' => 'GVO S2',
        'BIO_S1' => 'BIO S1',
        'BIO_S2' => 'BIO S2',
      ];
    }

  }

  /**
   * @return string[]
   */
  static function getFieldTitles() {
    return [
      'stop1' => '|-------------------------< GENERAL >-------------------------|',
      'hpscreg_name' => 'hPSCreg Name',
      'lab_id' => 'Lab ID',
      'alt_id' => 'Alternative Name',
      'editing_id' => 'Editing ID',
      'provider' => 'Provider',
      'location' => 'Location',
      'project' => 'Project',
      'cell_type' => 'Cell Type',
      'ipsc_id' => 'iPSC ID',
      'grade' => 'Grade',
      'same_donor' => 'Does an other cell line from the same donor exist?',
      'underlying_cell_line' => 'Underlying Cell Line',
      'related_cell_lines' => 'Related Cell Lines',
      'same_donor_line' => 'Cell Line(s) of Same Donor',
      'clone_used' => 'Clone Used',
      'obtainable' => 'Is the cell line readily obtainable for third parties?',
      'restric_areas' => 'Consent only for specific areas/projects?',
      'restric_research' => 'Consent for research use?',
      'restric_clinical' => 'Consent for clinical use?',
      'restric_commercial' => 'Consent for commercial use?',
      'restric_additional' => 'Additional restrictions?',
      'stop2' => '|-------------------------< EDITING >-------------------------|',
      'gen_predis_underlying' => 'Genetic Predisposition of Underlying Cell Line',
      'editing_strategy' => 'Editing Strategy',
      'editing_targeted_gene' => 'Targeted Gene',
      'visibility_targeted_gene' => 'Visibility of Targeted Gene',
      'editing_method' => 'Editing Method',
      'editing_date' => 'Editing Date',
      'stop3' => '|-------------------------< CLONES >-------------------------|',
      'clone_number' => 'Clone',
      'clone_number_starlims' => 'Clone Number in StarLIMS',
      'genomic_modification' => 'Genomic Modification',
      'hdr_nhej' => 'HDR/NHEJ',
      'zygosity' => 'Zygosity',
      'specification' => 'Specification',
      'spec_visibility' => 'Visibility of Specification',
      'clone_visibility' => 'Visibility of Clone',
      'culture_medium' => 'Culture Medium',
      'characterization' => 'Characterization',
      'start_date' => 'Start Date',
      'responsible' => 'Responsible',
      'morphology' => 'Morphology',
      'pluriotency_pcr' => 'Pluripotency PCR',
      'pluriotency_immuno' => 'Pluripotency Immuno',
      'pluriotency_facs' => 'Pluripotency FACS',
      'differentiation_immuno' => 'Differentitation Immuno',
      'hla_analyzed' => 'HLA analyzed?',
      'mutation_analyzed' => 'Mutation confirmed?',
      'str_analyzed' => 'STR analyzed?',
      'editing_analyzed' => 'Editing confirmed?',
      'karyo_analyzed' => 'Karyotype analyzed?',
      'karyotype' => 'Karyotype',
      'stop4' => '|------------------------< DIAGNOSIS >------------------------|',
      'primary_diagnosis' => 'Primary Diagnosis',
      'secondary_diagnosis' => 'Secondary Diagnosis',
      'disease_name' => 'Disease Name',
      'icd' => 'ICD',
      'genetic_predisposition' => 'Genetic Predisposition',
      'genetic_predisposition_spec' => 'Specification',
      'visibility_genetic_predisposition' => 'Visibility of Specification',
      'stop5' => '|--------------------------< DONOR >--------------------------|',
      'donor_gender' => 'Gender',
      'donor_race' => 'Race',
      'donor_age' => 'Age at Donation',
      'hla_a' => 'HLA-A of Donor',
      'hla_b' => 'HLA-B of Donor',
      'hla_drb1' => 'HLA-DRB1 of Donor',
      'donor_secutrial_id' => 'SecuTrial ID',
      'donor_pseudonym' => 'Pseudonym',
      'donor_starlims_id' => 'STARLIMS ID',
      'relative_combined' => 'Relative',
      'relative_type' => 'Relative Type',
      'relative_starlims_id' => 'Relative STARLIMS ID',
      'relative_pseudonym' => 'Relative Pseudonym',
      'stop6' => '|-----------------------< REPROGRAMMING >-----------------------|',
      'source_type' => 'Source Type',
      'sample_clinic' => 'Sample Clinic',
      'sampling_date' => 'Sampling Date',
      'primary_culture' => 'Is the primary culture available?',
      'reprogramming_method' => 'Reprogramming Method',
      'reprogramming_vector' => 'Reprogramming Vector',
      'reprogramming_date' => 'Reprogramming Date',
      'stop7' => '|----------------------< VIRUS TESTING >----------------------|',
      'hiv_result' => 'HIV - Result',
      'hiv_material' => 'HIV - Material',
      'hiv_date' => 'HIV - Date',
      'hep_b_result' => 'Hepatitis B - Result',
      'hep_b_material' => 'Hepatitis B - Material',
      'hep_b_date' => 'Hepatitis B - Date',
      'hep_c_result' => 'Hepatitis C - Result',
      'hep_c_material' => 'Hepatitis C - Material',
      'hep_c_date' => 'Hepatitis C - Date',
      'stop8' => '|--------------------------< ETHICS >--------------------------|',
      'ethics1' => 'Has consent been obtained from the donor of the tissue from which iPS cells have been derived?',
      'ethics2' => 'Was the consent voluntarily given by the donor, custodian or parents?',
      'ethics3' => 'Has the donor been informed that participation will not directly influence their personal treatment?',
      'ethics4' => 'Can you provide a copy of the Donor Information Sheet provided to the donor?',
      'ethics4_contact' => 'Contact Information',
      'ethics5' => 'Do you hold a copy of the signed Donor Consent Form?',
      'ethics5_contact' => 'Contact Information',
      'ethics6' => 'Please indicate whether the donated material has been pseudonymised or anonymised.',
      'ethics7' => 'Does consent expressly prevent derivation of iPS cells?',
      'ethics8' => 'Does consent prevent cells derived from the donated biosample from
       being made available to researchers anywhere in the world?',
      'ethics9' => 'How may genetic information associated with the cell line be accessed?',
      'ethics10' => 'Will the donor expect to receive financial benefit, beyond reasonable expenses,
      in return for donating the biosample?',
      'ethics11' => 'Has a favourable opinion been obtained from a research ethics committee, or other ethics review panel,
      in relation to the Research Protocol including the consent provisions?',
      'ethics11_spec_name' => 'Name of the accrediting authority involved',
      'ethics11_spec_number' => 'Approval Number',
      'ethics12' => 'Has a favourable opinion been obtained from a research ethics committee, or other ethics review panel,
      in relation to the proposed project, involving use of donated material or derived cells?',
      'ethics12_spec_name' => 'Name of the accrediting authority involved',
      'ethics12_spec_number' => 'Approval Number ',
      'ethics13' => 'Has consent been obtained for the storage of the biopsied tissue and relevant patient data?',
      'ethics14' => 'Has consent been obtained for involved physicians to have access to the biopsied tissue and relevant patient data?',
      'ethics15' => 'Has consent been obtained for the storage of the biopsied tissue and derived iPS cells in the UMG Biobank for further use in scientific research?',
      'ethics16' => 'Has consent been obtained for the donated tissue to be passed into the ownership of the UMG?',
      'ethics17' => 'Has consent been obtained for the distribution of the biopsied tissue and products derived thereof to scientific cooperation partners?',
      'ethics18' => 'Has consent been obtained for the potential commercial use of the biopsied tissue and products derived thereof?',
      'ethics19' => 'Has consent been obtained for the pseudonymized storage and use of the biopsied tissue, products derived thereof, as well as the corresponding patient data for 15 years?',
      'ethics20' => 'Has consent been obtained for the collection of a blood sample to test for viral infections?',
      'ethics21' => 'Has consent been obtained for genetic and epigenetic testing?',
      'ethics22' => 'Does the tissue donor wish to be informed about potential medically relevant genetic findings? ',
      'ethics23' => 'Has the donor agreed to the disposal of the primary tissue after 15 years?',
      'ethics24' => 'Has the donor agreed to the anonymization of products and data derived from the donated tissue? ',
      'ethics25' => 'Has the donor been informed of their right to terminate their participation in this trial?',
      'ethics26' => 'Has the donor been informed that a destruction of the cell products and data obtained from their donation is not possible retrospectively?',
      'ethics27' => 'Has consent been obtained to contact relatives of the donor? ',
      'stop9' => '|-----------------------< MISCELLANEOUS >-----------------------|',
      'contact' => 'Contact',
      'correspondence' => 'Correspondence for Distribution',
      'cost_coverage' => 'Correspondence for Cost Coverage',
      'distribution' => 'Distribution',
      'lines_in_biobank' => 'Cell Lines in UMG Biobank',
      'biosafety' => 'Biosafety Level',
      'comments' => 'Comments',
      'visibility' => 'Visibility',
      'tab_type' => 'Tab Type',
      'stop10' => '|--------------------------< BLOCKS >--------------------------|',
      'audit_trail' => 'Audit Trail',
      'linked_publications' => 'Linked Publications',
      'samples' => 'Samples',
      'starlims_clones' => 'Clones in UMG Biobank',
    ];
  }

  /**
   * @return string[]
   */
  static function getFieldTitlesSamples() {
    return [
      'type_sample' => 'Type of Sample',
      'type_primary_container' => 'Type of Primary Container',
      'precentrifugation' => 'Precentrifugation',
      'centrifugation1' => 'First Centrifugation',
      'centrifugation2' => 'Second Centrifugation',
      'postcentrifugation' => 'Postcentrifugation',
      'long_term_storage' => 'Long Term Storage',
      'amount_left' => 'Amount Left',
      'container_external_id' => 'Container External ID',
      'container_inventory_id' => 'Container Inventory ID',
      'container_pos_x' => 'Container X-Position',
      'container_pos_y' => 'Container Y-Position',
      'internal_inventory_id' => 'Internal Inventory ID',
      'kit_id' => 'Kit ID',
      'location' => 'Location',
      'material_code' => 'Material Code',
      'project_name' => 'Project Name',
      'sample_container' => 'Sample Container',
      'sample_id' => 'Sample ID',
      'tube_barcode' => 'Tube Barcode',
    ];
  }

  /**
   * @return string[]
   */
  static function getFieldTitlesStarlimsClones() {
    return [
      'clone_name' => 'Name',
      'starlims_id' => 'StarLIMS ID',
      'starlims_lab_id' => 'Lab ID in StarLIMS',
      'tubes' => '#Tubes',
    ];
  }

  /**
   * @return string[]
   */
  static function getOptionsIcd() {
    $diseases_objects = CellLineDiseaseRepository::findAll();
    $icd = [];
    foreach ($diseases_objects as $disease_object) {
      $icd[] = $disease_object->getIcd();
    }
    asort($icd);
    return arrayValueToKey($icd);
  }

  /**
   * @return string[]
   */
  static function getOptionsDiseaseNames() {
    $diseases_objects = CellLineDiseaseRepository::findAll();
    $disease_names = [];
    foreach ($diseases_objects as $disease_object) {
      $disease_names[] = $disease_object->getDiseaseName();
    }
    asort($disease_names);
    return arrayValueToKey($disease_names);
  }

  /**
   * @return string
   */
  static function getOptionNa() {
    return '<i>n/a</i>';
  }
}

/**
 * Takes the values of a given array and set them as keys AND values of a new
 * array (it "overrides" the keys of an array wih the values of the array).
 *
 * @param mixed[] $array
 *   The array in which keys and values are to be the same.
 *
 * @return array
 *   The final array.
 */
function arrayValueToKey($array) {
  return array_combine($array, $array);
}

/**
 * Takes the biosafety db key/value and transforms it into a readable String.
 *
 * @param $db_key
 *   Value in the DB.
 * @return string|null
 *   Visual String.
 */
function showBiosafety ($db_key) {
  if ($db_key == 'S1') {
    return 'GVO S1';
  }
  elseif ($db_key == 'S2') {
    return 'GVO S2';
  }
  elseif ($db_key == 'BIO_S1') {
    return 'BIO S1';
  }
  elseif ($db_key == 'BIO_S2') {
    return 'BIO S2';
  } else {
    return NULL;
  }
}
