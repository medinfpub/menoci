/**
 * Used for Select2 form field. Formats the search info boxes.
 */
function formatCellmodels(repo) {
  if (repo.loading) {
    return repo.text;
  }

  var markup = "" +
      "<div class='select2-result-repository clearfix'>" +
      "<div class='media'>" +
      "<div class='media-left'>" +
      "<img src='https://sfb1002.med.uni-goettingen.de/production/sites/all/themes/sfb-theme-generic/img/Icon_CMC.png' height='32' width='32'>" +
      "</div>" +
      "<div class='media-body'><h4 class='media-heading'>" +  repo.hpscreg_name + "</h4>" +
      "<dl class='dl-horizontal'>" +
      "<dt><small>Cell Type:</small></dt><dd><small>" + repo.cell_type + "</small></dd>" +
      "<dt><small>ICD Code:</small></dt><dd><small>" + repo.icd + "</small></dd>" +
      "<dt><small>Disease:</small></dt><dd><small>" + repo.disease + "</small></dd>" +
      "<dt><small>Source:</small></dt><dd><small>" + repo.source + "</small></dd>" +
      "<dt><small>Reprogramming:</small></dt><dd><small>" + repo.reprogramming + "</small></dd>" +
      "<dt><small>Grade:</small></dt><dd><small>" + repo.grade + "</small></dd>" +
      "</dl>" +
      "</div>" +
      "</div>" +
      "</div>";

  return markup;
}

/**
 * Used for Select2 from field. Formats the selection tags/boxes.
 */
function formatCellmodelsSelection(repo) {
  return repo.hpscreg_name || repo.text;
}
