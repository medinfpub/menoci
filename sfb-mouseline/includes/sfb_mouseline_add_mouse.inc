<?php
/**
 * Created by PhpStorm.
 * User: Paul_Roman
 * Date: 10.01.2017
 * Time: 15:58
 */
/**
 * returns a form for adding mice to a mouse line. the form is dynamically generated using ajax callbacks.
 *
 * @param $form
 * @param $form_state
 * @return mixed
 */
function sfb_mouseline_add_mouse($form, &$form_state, $mouseline_PID){

  $form_state['mouseline_id'] = Mouseline::extractIDFromPID($mouseline_PID);

  //
  // prepare new mouse form
  //
  $form['description'] = array(
      '#markup' => '<div>' . t('')
          . '</div>',
  );

  //get array of mouseline's mice
  $mice = MouselineMouseRepository::findByMouselineId($form_state['mouseline_id']);

  //if there was a recent change to the number due to an ajax callback, this flag is set as true
  if(!isset($form_state['flag_ajax'])) {
    $form_state['num_mice'] = sizeof($mice);
    if (empty($form_state['num_mice']) || $form_state['num_mice'] == 0)
      $form_state['num_mice'] = 1;
  }

  // Because we have many fields with the same values, we have to set
  // #tree to be able to access them.
  $form['#tree'] = TRUE;
  $form['fieldset-mice'] = array(
      '#type' => 'fieldset',
      '#title' => '<span class="fa fa-info"></span> ' . t('Insert Mice Information'),
    // Set up the wrapper so that AJAX will be able to replace the fieldset.
      '#prefix' => '<div id="names-fieldset-wrapper">',
      '#suffix' => '</div>',
      '#title_display' => 'invisible',
  );
  $form['add_name'] = array(
      '#type' => 'submit',
      '#value' => t('Add another mouse'),
      '#submit' => array('ajax_example_add_more_add_one'),
      '#ajax' => array(
          'callback' => 'ajax_example_add_more_callback',
          'wrapper' => 'names-fieldset-wrapper',
          'effect' =>'fade',
      ),
      '#limit_validation_errors' => array(),
  );
  $form['fieldset-mice']['table-prefix'] = array(
      '#type' => 'markup',
      '#markup' => '<table class="table"><thead><tr>
                    <th>Name</th>
                    <th>Sex</th>
                    <th>Date of Birth</th>
                    <th>Date of Death</th>
                    <th>Sharing Level</th>
                    <th>Comments</th>
                </tr></thead> <tbody>'
  );

  for ($i = 0; $i < $form_state['num_mice']; $i++){
    if(empty($mice[$i]))
      $mouse = new MouselineMouse();
    else
      $mouse = $mice[$i];
    //Fill Table Row
    $form['fieldset-mice']['mouse-'.$i]['name'] = $mouse->getFormFieldName(true, '<tr><td>', '</td>', 'invisible');
    $form['fieldset-mice']['mouse-'.$i]['sex'] = $mouse->getFormFieldSex(true, '<td>', '</td>', 'invisible');
    $form['fieldset-mice']['mouse-'.$i]['date-of-birth'] = $mouse->getFormFieldDateOfBirth(true, '<td>', '</td>', 'invisible');
    $form['fieldset-mice']['mouse-'.$i]['date-of-death'] = $mouse->getFormFieldDateOfDeath(false, '<td>', '</td>', 'invisible');
    $form['fieldset-mice']['mouse-'.$i]['sharing-level'] = $mouse->getFormFieldSharingLevel('<td>', '</td>', 'invisible');
    $form['fieldset-mice']['mouse-'.$i]['comments'] = $mouse->getFormFieldComments(false, '<td>', '</td>', 'invisible');
  }

  $form['fieldset-mice']['table-suffix'] = array(
      '#type' => 'markup',
      '#markup' => '</tbody></table>'
  );

  $form['cancel'] = array(
      '#type' => 'submit',
      '#value' => t('Cancel'),
      '#submit' => array('sfb_mouseline_add_mouse_cancel'),
      '#validate' => array(),
    '#limit_validation_errors' => array()
  );

  $form['save'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
  );

  if ($form_state['num_mice'] > 1) {
    $form['fieldset-mice']['remove_name'] = array(
        '#type' => 'submit',
        '#value' => t('Remove last entry'),
        '#submit' => array('ajax_example_add_more_remove_one'),
        '#ajax' => array(
            'callback' => 'ajax_example_add_more_callback',
            'wrapper' => 'names-fieldset-wrapper',
        ),
        '#limit_validation_errors' => array(),

    );
  }

  return $form;
}

/**
 * save mice after hitting the submit button
 *
 * @param $form
 * @param $form_state
 */
function sfb_mouseline_add_mouse_submit($form, &$form_state) {
  for ($i = 0; $i < $form_state['num_mice']; $i++) {
    $mouse = new MouselineMouse();
    $mouseline = MouselineRepository::findById($form_state['mouseline_id']);
    $mouse->setName($form_state['values']['fieldset-mice']['mouse-'.$i]['name']);
    $mouse->setSex($form_state['values']['fieldset-mice']['mouse-'.$i]['sex']);
    $mouse->setMouselineId($mouseline->getId());
    $mouse->setDateOfBirth($form_state['values']['fieldset-mice']['mouse-'.$i]['date-of-birth']);
    $mouse->setDateOfDeath($form_state['values']['fieldset-mice']['mouse-'.$i]['date-of-death']);
    $mouse->setWorkingGroupId($mouseline->getWorkingGroupId());
    $mouse->setSharingLevel($form_state['values']['fieldset-mice']['mouse-'.$i]['sharing-level']);
    $mouse->setComments($form_state['values']['fieldset-mice']['mouse-'.$i]['comments']);
    $mouse->setCreatedDate(date('Y-m-d H:i:s'));
    $mouse->save();
  }

  $form_state['redirect'] = sfb_mouseline_url(SFB_MOUSELINE_URL_VIEW_MOUSELINE, arg(1));
}

/**
 * sends the user back
 *
 * @param $form
 * @param $form_state
 */
function sfb_mouseline_add_mouse_cancel($form, &$form_state) {
  $form_state['redirect'] = sfb_mouseline_url(SFB_MOUSELINE_URL_VIEW_MOUSELINE, arg(1));
}

/***
 *
 *  Ajax Callbacks
 * Callback for both ajax-enabled buttons.
 *
 */
/**
 * @param $form
 * @param $form_state
 * @return mixed
 */
function ajax_example_add_more_callback($form, $form_state) {
  return $form['fieldset-mice'];
}

/**
 * Submit handler for the "add-one-more" button.
 *
 * Increments the max counter and causes a rebuild.
 *
 * @param $form
 * @param $form_state
 */
function ajax_example_add_more_add_one($form, &$form_state) {
  $form_state['num_mice']++;
  $form_state['flag_ajax'] = TRUE;
  $form_state['rebuild'] = TRUE;
}

/**
 * Submit handler for the "remove one" button.
 *
 * Decrements the max counter and causes a form rebuild.
 *
 * @param $form
 * @param $form_state
 */
function ajax_example_add_more_remove_one($form, &$form_state) {
  if ($form_state['num_mice'] > 1) {
    $form_state['num_mice']--;
  }
  $form_state['rebuild'] = TRUE;
}
/*
function sfb_mouseline_remove_mouse($form, &$form_state) {
  if(isset($form_state['triggering_element']['#parents'][1])) {
    drupal_set_message($form_state['triggering_element']['#parents'][1]);
    $arg_array = explode('-', $form_state['triggering_element']['#parents'][1]);
    $id = $arg_array[1];
    drupal_set_message($id);


    if (EchoSeriesRepository::deleteById($id) > 0)
      drupal_set_message('Series ' . $form_state['series'][$id]['id'] . ' successfully removed.');
    else
      drupal_set_message('Series ' . $form_state['series'][$id]['id'] . ' could not be removed.');


    unset($form_state['series'][$id]);

    $form_state['num_names']--;

    if($form_state['num_names'] == 0)
      ajax_example_add_more_add_one($form, $form_state);


    $form_state['rebuild'] = TRUE;
  }
}*/