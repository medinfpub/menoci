<?php
/**
 * Created by PhpStorm.
 * User: Paul_Roman
 * Date: 13.01.2017
 * Time: 13:06
 */
/**
 *  Main API handler.
 */
function sfb_mouseline_api() {

  // Autocomplete paths are not specific for any api version

  $api = arg(2);        // version of the api called or 'autocomplete'
  $call = arg(3);       // api call
  $parameter1 = arg(4); // optional: parameter1
  $parameter2 = arg(5); // optional: parameter1

  if($api == '2') {

    switch($call) {
      case 'mouseline':
        return sfb_mouseline_api_mouseline($parameter1);
      case 'mouse':
        return sfb_mouseline_api_mouse($parameter1);
      default:
        return 'Unknown api call';
    }


  } else if ($api == 'autocomplete') {
    switch($call) {
      case 'inbred':
        return sfb_mouseline_api_autocomplete_inbred($parameter1);
      case 'lab':
        return sfb_mouseline_api_autocomplete_lab($parameter1);
      case 'strain':
        return sfb_mouseline_api_autocomplete_strain($parameter1);
    }

  } else {
    drupal_not_found();
    exit();
  }
}


/**
 * API handler for mouselines
 *
 * @param string $string
 */
function sfb_mouseline_api_mouseline($string = '') {
  $matches = array();
  if($string) {
    $db_or = db_or();
    $db_or->condition('id',Mouseline::extractIDFromPID($string),'=');
    $db_or->condition('standard_name', '%' . db_like($string).'%','LIKE');


    $mouselines = MouselineRepository::findBy($db_or);

    foreach($mouselines as $mouseline) {
      $matches[] = array(
        'version' => SFB_MOUSELINE_API_VERSION,
        'id' => check_plain($mouseline->getId()),
        'type' => check_plain($mouseline->getType()),
        'pid' => check_plain($mouseline->getElementPID()),
        'name' => check_plain($mouseline->getName()),
      );
    }
  }

  drupal_json_output($matches);
}

/**
 * API handler for mice
 *
 * @param string $string
 */
function sfb_mouseline_api_mouse($string = '') {
  drupal_set_message("HIER: ".$string);
  $matches = array();
  if($string) {
    $db_or = db_or();
    $db_or->condition('id',MouselineMouse::extractIDFromPID($string),'=');
    $db_or->condition('name',db_like($string).'%','LIKE');


    $mice = MouselineMouseRepository::findBy($db_or);

    foreach($mice as $mouse) {
      $matches[] = array(
          'version' => SFB_MOUSELINE_API_VERSION,
          'id' => check_plain($mouse->getId()),
          'sex' => check_plain($mouse->getSex()),
          'pid' => check_plain($mouse->getElementPID()),
          'name' => check_plain($mouse->getName()),
      );
    }
  }

  drupal_json_output($matches);
}


/**
 * completes the names of inbred strains after being geiven the first 2 letters. fetches possible selection from db
 *
 * @param string $string
 */
function sfb_mouseline_api_autocomplete_inbred($string = '') {
  $matches = array();
  if($string) {
    $db_or = db_or();
    $db_or->condition('standard_name', db_like($string).'%','LIKE');

    $lines = MouselineRepository::findBy($db_or);

    foreach($lines as $line) {
      $name = check_plain($line->getName());
      $matches[check_plain($name.', PID: ' .check_plain($line->getElementPID()))] = check_plain($line->getName().', PID: ' .check_plain($line->getElementPID()));
    }

  }

  drupal_json_output($matches);
}

/**
 * completes the names of labs after being geiven the first letters. fetches possible selection from db
 *
 * @param string $string
 */
function sfb_mouseline_api_autocomplete_lab($string = '') {
  $matches = array();
  if($string) {
    $db_or = db_or();
    $db_or->condition('name', db_like($string).'%','LIKE');

    $lines = MouselineLaboratoryRepository::findBy($db_or);

    foreach($lines as $line) {
      $matches[check_plain($line->getName())] = check_plain($line->getName());
    }

  }

  drupal_json_output($matches);
}

/**
 * completes the names of strains after being given the first letters. fetches possible selection from db
 *
 * @param string $string
 */
function sfb_mouseline_api_autocomplete_strain($string = '') {
  $matches = array();
  if($string) {
    $db_or = db_or();
    $db_or->condition('name', db_like($string).'%','LIKE');

    $lines = MouselineStrainRepository::findBy($db_or);

    foreach($lines as $line) {
      $matches[check_plain($line->getName())] = check_plain($line->getName());
    }

  }

  drupal_json_output($matches);
}