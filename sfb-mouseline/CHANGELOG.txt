SFB Mouseline 0.xx, xxxx-xx-xx (development version)
-----------------------
- Fixed issues with regular expressions (R. Paul)
- Implemented new help page (prototype) (R. Paul)
- Reworked "Add mice" page
- Added antibody linkage
- 

SFB Mouseline 0.1, 2017-02-06
-----------------------
- Initial release
- (Bachelor thesis R. Paul)