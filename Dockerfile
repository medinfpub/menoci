FROM drupal:7-apache

# Install PHP extension: imagemagick
RUN apt-get update && \
    apt-get install -y libmagickwand-dev --no-install-recommends && \
    pecl install imagick && \
    docker-php-ext-enable imagick && \
    apt-get clean

# Install the tools git, wget, unzip
RUN apt-get update && apt-get install -y git wget unzip --no-install-recommends && \
    apt-get clean

# Install PHP extension mysqli which is required for the research data archive
RUN docker-php-ext-install mysqli && \
    docker-php-ext-enable mysqli

# Install composer
RUN curl -sS https://getcomposer.org/installer | \
    php -- --install-dir=/usr/bin/ --filename=composer

# Install msmtp for mail forwarding to another SMTP server
# /etc/msmtprc configuration file needs to be mapped into the container
RUN apt-get update && apt-get install -y msmtp --no-install-recommends && \
    apt-get clean \
    && ln -s /usr/bin/msmtp /usr/sbin/sendmail

#
# Install Drupal theme dependencies
#
USER www-data:www-data

WORKDIR /var/www/html/sites/all/themes

RUN wget https://ftp.drupal.org/files/projects/bootstrap-7.x-3.27.tar.gz \
    && tar xzfv bootstrap-7.x-3.27.tar.gz \
    && rm bootstrap-7.x-3.27.tar.gz

RUN wget https://gitlab-pe.gwdg.de/research-data-platform/menoci-theme/-/archive/master/menoci-theme-master.tar.gz \
    && tar xzfv menoci-theme-master.tar.gz \
    && rm menoci-theme-master.tar.gz 
# \
#    && mv menoci-theme-master menoci-theme

#
# Download Bootstrap library distribution files for local delivery instead of third-party CDN usage
#   Library files will be available to the Drupal engine under relative path
#   sites/all/libraries/bootstrap/
#
WORKDIR /var/www/html/sites/all/libraries

RUN wget https://github.com/twbs/bootstrap/releases/download/v3.4.1/bootstrap-3.4.1-dist.zip \
    && unzip bootstrap-3.4.1-dist.zip \
    && mv bootstrap-3.4.1-dist bootstrap \
    && rm bootstrap/css/bootstrap-theme* \
    && rm bootstrap-3.4.1-dist.zip

#
# Install Drupal module dependencies
#
WORKDIR /var/www/html/sites/all/modules

RUN wget https://ftp.drupal.org/files/projects/jquery_update-7.x-4.1.tar.gz \
    && tar xzfv jquery_update-7.x-4.1.tar.gz \
    && rm jquery_update-7.x-4.1.tar.gz

RUN wget https://ftp.drupal.org/files/projects/entity-7.x-1.11.tar.gz \
    && tar xzfv entity-7.x-1.11.tar.gz \
    && rm entity-7.x-1.11.tar.gz

RUN wget https://ftp.drupal.org/files/projects/profile2-7.x-1.7.tar.gz \
    && tar xzfv profile2-7.x-1.7.tar.gz \
    && rm profile2-7.x-1.7.tar.gz

# Depricated in php8
#RUN wget https://ftp.drupal.org/files/projects/account_profile-7.x-2.0.tar.gz \
#    && tar xzfv account_profile-7.x-2.0.tar.gz \
#    && rm account_profile-7.x-2.0.tar.gz
# new account_profile for php8
RUN git clone -b main --single-branch https://gitlab+deploy-token-827:jxQzwry49MhVksoKXr-C@gitlab.gwdg.de/research-data-platform/extended-account-profile.git \
    && mv extended-account-profile account_profile


RUN wget https://ftp.drupal.org/files/projects/date-7.x-2.14.tar.gz \
    && tar xzfv date-7.x-2.14.tar.gz \
    && rm date-7.x-2.14.tar.gz

RUN wget https://ftp.drupal.org/files/projects/miniorange_oauth_client-7.x-1.353.tar.gz \
    && tar xzfv miniorange_oauth_client-7.x-1.353.tar.gz \
    && rm miniorange_oauth_client-7.x-1.353.tar.gz

RUN wget https://ftp.drupal.org/files/projects/site_alert-7.x-1.0.tar.gz \
    && tar xzfv site_alert-7.x-1.0.tar.gz \
    && rm site_alert-7.x-1.0.tar.gz


#
# Install menoci modules
#
WORKDIR /var/www/html/sites/all/modules
RUN mkdir menoci
ADD ./ menoci/
WORKDIR /var/www/html/sites/all/modules/menoci
RUN ./update.sh
WORKDIR /var/www/html/sites/all/modules
# Clean up
USER root
RUN find menoci/ -type f -maxdepth 1 -delete && chown www-data: -R .

# Install RDP module dependencies
USER www-data:www-data
RUN cd menoci/rdp-wikidata && composer install
RUN cd menoci && git submodule init rdp-archive/cdstar/lib/jwt/ \
    && git submodule update rdp-archive/cdstar/lib/jwt/
# Clean up
RUN find . -maxdepth 1 -type f -delete && rm -rf menoci/.git

#
# All done; set workdir to webroot
#
WORKDIR /var/www/html/

#
# Add a custom php.ini file for Drupal project
#
RUN echo "upload_max_filesize = 50M" > php.ini \
    && echo "post_max_size = 50M" >> php.ini \
    && echo "sendmail_path = '/usr/sbin/sendmail -t -i'" >> php.ini

USER root:root
